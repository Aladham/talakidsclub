<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('order_p');
            $table->double('price');
            $table->float('tax');
            $table->binary('online_party')->nullable();
            $table->integer('fk_product_type_id')->unsigned();
            $table->timestamps();

            $table->index(["fk_product_type_id"], 'product_type_id_idx');


            $table->foreign('fk_product_type_id', 'product_type_id_idx')
                ->references('id')->on('product_types')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
