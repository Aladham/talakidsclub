<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_data', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('credit')->nullable();
            $table->integer('hour_free')->nullable();
            $table->integer('points')->nullable();
            $table->string('notes', 250)->nullable();
            $table->dateTime('last_visit')->nullable();
            $table->integer('fk_customer_id')->unsigned();;
            $table->timestamps();
            $table->index(["fk_customer_id"], 'fk_customer_id_idx');


            $table->foreign('fk_customer_id', 'fk_customer_id_idx')
                ->references('id')->on('customers')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_data');
    }
}
