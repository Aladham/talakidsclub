<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('party_products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description', 250)->nullable();
            $table->integer('order_p');
            $table->double('price');
            $table->integer('tax');
            $table->double('deposit')->nullable();
            $table->double('price_per_extra_child')->nullable();
            $table->integer('size');
            $table->string('duration', 45);
            $table->integer('fk_product_type_id')->unsigned();
            $table->timestamps();
            $table->index(["fk_product_type_id"], 'fk_product_type_id_idx');


            $table->foreign('fk_product_type_id', 'fk_product_type_id_idx')
                ->references('id')->on('product_types')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('party_products');
    }
}
