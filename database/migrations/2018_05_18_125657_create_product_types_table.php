<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('fk_sale_tab_id')->unsigned();
            $table->integer('parties')->default(0);
            $table->string('button_colour', 45);
            $table->integer('weight');
            $table->timestamps();

            $table->index(["fk_sale_tab_id"], 'sale_tab_id_idx');


            $table->foreign('fk_sale_tab_id', 'sale_tab_id_idx')
                ->references('id')->on('sale_tabs')
                ->onDelete('no action')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
}
