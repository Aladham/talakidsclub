<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temps', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('item');
            $table->float('price');
            $table->float('tax');
            $table->integer('quantity');
            $table->integer('id_sale')->unsigned();
            $table->float('discount');
            $table->string('discount_reasons')->nullable();;
            $table->integer('fk_customer_id')->unsigned();
            $table->integer('fk_users_id')->unsigned();
            $table->string('sale_notes')->nullable();;
            $table->integer('active')->default(0);
            $table->timestamps();

//
//            $table->foreign('id_sale')
//                ->references('id')->on('sales')
//                ->onDelete('no action')
//                ->onUpdate('no action');

            $table->foreign('fk_customer_id')
                ->references('id')->on('customers')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('fk_users_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::dropIfExists('temps');
    }
}
