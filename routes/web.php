<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/sale/');
});
//Route::get('sale', function () {
//    return view('sale');
//})->name('sale');


Auth::routes();
Route::get('/cashDrawer', 'HomeController@cashDrawer')->name('cashDrawer');


Route::group(['middleware' => ['CheckAdmin']], function () {
    Route::get('/admin', 'AdminController@adminPage')->name('adminPage');
    Route::get('/manage', 'HomeController@managePage')->name('managePage');

    Route::get('/customers/delete/{id}', 'CustomerController@deleteCustomer')->name('deleteCustomer', 'id');
    Route::get('/deleteVisit/delete/{id}', 'CustomerController@deleteVisit')->name('deleteVisit', 'id');
    Route::get('/deleteAllSaleCustomer/{id}', 'CustomerController@deleteAllSaleCustomer')->name('deleteAllSaleCustomer', 'id');
    Route::get('/deleteSaleCustomer/{id}', 'CustomerController@deleteSaleCustomer')->name('deleteSaleCustomer', 'id');
    Route::get('/SaleTab/delete/{id}', 'SaleTabController@deleteSaleTab')->name('deleteSaleTab', 'id');
    Route::post('/AddSaleTab', 'SaleTabController@AddSaleTab')->name('AddSaleTab');
    Route::get('/edit-SaleTab/{id}', 'SaleTabController@editSaleTab')->name('editSaleTab', 'id');
    Route::put('/update-SaleTab/{id}', 'SaleTabController@updateSaleTab')->name('updateSaleTab', 'id');
    Route::get('/activate-SaleTab/{id}', 'SaleTabController@activateSaleTab')->name('activateSaleTab', 'id');
    Route::get('/deactivate-SaleTab/{id}', 'SaleTabController@deactivateSaleTab')->name('deactivateSaleTab', 'id');
    Route::get('/ProductType', 'ProductTypeController@productType')->name('productType');
    Route::post('/AddProductType', 'ProductTypeController@addProductType')->name('AddProductType');
    Route::get('/edit-ProductType/{id}', 'ProductTypeController@editProductType')->name('editProductType', 'id');
    Route::get('/ProductType/delete/{id}', 'ProductTypeController@deleteProductType')->name('deleteProductType', 'id');
    Route::put('/update-ProductType/{id}', 'ProductTypeController@updateProductType')->name('updateProductType', 'id');
//    Route::get('/ActivateCustomers', 'CustomerController@getDeletedCustomers')->name('getDeletedCustomers');
//    Route::get('/ActivateCustomers/{id}', 'CustomerController@activateCustomer')->name('activateCustomer','id');
    Route::get('/SaleTab', 'SaleTabController@getSaleTabs')->name('SaleTab');

    Route::get('/Product', 'ProductController@getProduct')->name('getProduct');
    Route::post('/AddProduct', 'ProductController@addProduct')->name('AddProduct');
    Route::get('/edit-Product/{id}', 'ProductController@editProduct')->name('editProduct', 'id');
    Route::get('/Product/delete/{id}', 'ProductController@deleteProduct')->name('deleteProduct', 'id');
    Route::put('/update-Product/{id}', 'ProductController@updateProduct')->name('updateProduct', 'id');

    Route::get('/PartyProduct', 'PartyProductController@getPartyProduct')->name('getPartyProduct');
    Route::post('/AddPartyProduct', 'PartyProductController@AddPartyProduct')->name('AddPartyProduct');
    Route::get('/edit-PartyProduct/{id}', 'PartyProductController@editPartyProduct')->name('editPartyProduct', 'id');
    Route::get('/PartyProduct/delete/{id}', 'PartyProductController@deletePartyProduct')->name('deletePartyProduct', 'id');
    Route::put('/update-PartyProduct/{id}', 'PartyProductController@updatePartyProduct')->name('updatePartyProduct', 'id');

    Route::get('/Employees', 'EmployeeController@viewEmployees')->name('viewEmployees');
    Route::post('/AddEmployee', 'EmployeeController@AddEmployee')->name('AddEmployee');
    Route::get('/deleteEmployee/delete/{id}', 'EmployeeController@deleteEmployee')->name('deleteEmployee', 'id');
    Route::get('/editEmployee/{id}', 'EmployeeController@editEmployee')->name('editEmployee', 'id');
    Route::put('/updateEmployee/{id}', 'EmployeeController@updateEmployee')->name('updateEmployee', 'id');

    Route::get('/PaymentType', 'PaymentController@getPayment')->name('getPayment');
    Route::post('/addPaymentType', 'PaymentController@addPaymentType')->name('addPaymentType');
    Route::get('/deletePaymentType/delete/{id}', 'PaymentController@deletePaymentType')->name('deletePaymentType', 'id');
    Route::get('/editPaymentType/{id}', 'PaymentController@editPaymentType')->name('editPaymentType', 'id');
    Route::put('/update-PaymentType/{id}', 'PaymentController@updatePaymentType')->name('updatePaymentType', 'id');

    Route::get('/tax', 'TaxController@getTax')->name('getTax');
    Route::post('/addTaxRate', 'TaxController@addTaxRate')->name('addTaxRate');
    Route::get('/TaxRate/delete/{id}', 'TaxController@deleteTaxRate')->name('deleteTaxRate', 'id');
    Route::get('/editTaxRate/{id}', 'TaxController@editTaxRate')->name('editTaxRate', 'id');
    Route::put('/update-TaxRate/{id}', 'TaxController@updateTaxRate')->name('updateTaxRate', 'id');

    Route::get('/discount', 'DiscountController@getDiscount')->name('getDiscount');
    Route::post('/addDiscount', 'DiscountController@addDiscount')->name('addDiscount');
    Route::get('/discount/delete/{id}', 'DiscountController@deleteDiscount')->name('deleteDiscount', 'id');
    Route::get('/editDiscount/{id}', 'DiscountController@editDiscount')->name('editDiscount', 'id');
    Route::put('/update-Discount/{id}', 'DiscountController@updateDiscount')->name('updateDiscount', 'id');

    Route::get('/discountReason', 'DiscountController@getDiscountReason')->name('getDiscountReason');
    Route::post('/addDiscountReason', 'DiscountController@addDiscountReason')->name('addDiscountReason');
    Route::get('/discount-Reason/delete/{id}', 'DiscountController@deleteDiscountReason')->name('deleteDiscountReason', 'id');
    Route::get('/editDiscountReason/{id}', 'DiscountController@editDiscountReason')->name('editDiscountReason', 'id');
    Route::put('/update-DiscountReason/{id}', 'DiscountController@updateDiscountReason')->name('updateDiscountReason', 'id');

    Route::get('/hideSale/{id}', 'SaleController@hideSale')->name('hideSale', 'id');
    Route::get('/activeSale/{id}', 'SaleController@activeSale')->name('activeSale', 'id');
    Route::get('AllSale', 'SaleController@getAllSale')->name('getAllSale');

    Route::get('/SaleOfDay', 'SaleController@saleDay')->name('saleDay');
    Route::get('/SaleOfDays', 'SaleController@saleDays')->name('saleDays');
    Route::get('/deleteSaleDay', 'SaleController@deleteSaleDay')->name('deleteSaleDay');


    Route::put('/hideMultiSale', 'SaleController@hideMultiSale')->name('hideMultiSale');

    Route::get('/reports', 'ReportController@viewReports')->name('reports');
    Route::post('/dailySummaryReport', 'ReportController@dailySummaryReport')->name('dailySummaryReport');
    Route::post('/salesReportByProduct', 'ReportController@salesReportByProduct')->name('salesReportByProduct');
    Route::post('/summaryBey', 'ReportController@summaryBey')->name('summaryBey');

});


Route::get('/customers', 'CustomerController@getCustomers')->name('customers');
//Route::get('/api/customers', 'CustomerController@getCustomers')->name('getCustomersapi');

Route::get('/AddCustomer', 'CustomerController@view_add_customer')->name('ViewAddCustomer');
Route::get('/searchCustomers', 'CustomerController@searchCustomers')->name('searchCustomers');
Route::post('/AddCustomer', 'CustomerController@addCustomer')->name('AddCustomer');
Route::get('/edit-customer/{id}', 'CustomerController@editCustomer')->name('editCustomer', 'id');
Route::get('/customer/{id}', 'CustomerController@viewCustomer')->name('viewCustomer', 'id');
Route::put('/customers/{id}', 'CustomerController@updateCustomer')->name('updateCustomer', 'id');
Route::put('/point', 'CustomerController@usePoint')->name('usePoint');

Route::get('/customer/delete/{id}', 'CustomerController@deleteChild')->name('deleteChild', 'id');
Route::get('/edit-child/{id}', 'CustomerController@editChild')->name('editChild', 'id');
Route::put('/update-child/{id}', 'CustomerController@updateChild')->name('updateChild', 'id');
Route::get('/AddChild/{id}', 'CustomerController@ViewAddChild')->name('ViewAddChild', 'id');
Route::post('/AddChild/{id}', 'CustomerController@AddChild')->name('AddChild', 'id');


Route::get('/sale/create/{id?}', 'TempController@createSale')->name('sale', 'id');
Route::get('/sale/{id?}', 'TempController@deleteAllSaleTempForAuth')->name('saleEmpty');
Route::post('/addSaleTemp', 'TempController@addSaleTemp')->name('addSaleTemp');
Route::get('/sale/customer/delete/{id}', 'TempController@deleteSaleTemp')->name('deleteSaleTemp', 'id');
Route::get('/sale/customer/Create/{id}', 'TempController@deleteAllSaleTemp')->name('deleteAllSaleTemp', 'id');
Route::put('/addDiscountItem', 'TempController@addDiscountItem')->name('addDiscountItem');
Route::put('/addQuantityItem', 'TempController@addQuantityItem')->name('addQuantityItem');


Route::post('/depositSale', 'TempController@depositSale')->name('depositSale');

Route::get('/DataSale/{id}', 'SaleController@getSale')->name('DataSale', 'id');
Route::get('/editSale/{id}', 'SaleController@editSale')->name('editSale', 'id');
Route::put('/updateSale/{id}', 'SaleController@updateSale')->name('updateSale', 'id');
Route::get('/SaleData/{id}', 'SaleController@getSaleTax')->name('getSaleTax', 'id');


Route::post('position', 'ProductController@position')->name('position');

Route::get('/search', 'HomeController@search')->name('search');


Route::get('/productSearch', 'ProductController@productSearch')->name('productSearch');


Route::get('/searchSale', 'SaleController@searchSale')->name('searchSale');
Route::get('/receipts/{id}', 'TempController@Receipts')->name('receipts', 'id');


Route::get('/parties', 'EventsController@parties')->name('parties');
Route::get('/addParty', function () {
    return view('parties.addParty');
})->name('viewAddParty');
Route::post('/addParty', 'EventsController@addParty')->name('addParty');

Route::get('/party/{id}', 'EventsController@viewParty')->name('viewParty','id');
Route::get('/editParty/{id}', 'EventsController@editParty')->name('editParty','id');
Route::put('/update-party/{id}', 'EventsController@updateParty')->name('updateParty', 'id');
Route::get('/delete-party/{id}', 'EventsController@deleteParty')->name('deleteParty', 'id');
