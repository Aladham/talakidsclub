$(document).ready(function () {
    $('.table').DataTable({
        "pageLength": 100,
        bSort: false,
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ]
    });

});