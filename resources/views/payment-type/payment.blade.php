@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Payment Types</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('adminPage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('addPaymentType')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="payment_type" class="col-sm-4 col-form-label">Payment Type:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="payment_type" name="payment_type">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="payment_name" class="col-sm-4 col-form-label">Payment Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="payment_name" name="payment_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Payment Type</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if(count($data)>0)
        @if(session()->has('messageDelete'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('messageDelete')}}</span>
            </div>
        @endif
        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Payment Type</th>
                    <th scope="col">Display Name</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td>{{$row->payment_type}}</td>
                        <td>{{$row->payment_name}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('editPaymentType',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deletePaymentType',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
        @endif
        <hr>

    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        $('.btn-package').on('click', function () {
            $('.add_package').fadeToggle(500);
        });
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection