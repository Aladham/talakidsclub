@extends('layouts.app')

@section('content')


    <nav class="sale">
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <?php $i = 0 ?>
            @foreach($data as $row)
                <a class="nav-item nav-link <?php if ($i == 0) {
                    echo "active";
                } ?> " id="nav-home-tab" data-toggle="tab" href="#nav-{{$row->id}}" role="tab"
                   aria-controls="nav-home" aria-selected="true">
                    {{$row->name}}
                </a>
                <?php $i++;?>
            @endforeach

        </div>
    </nav>

    <div class="row no-gutters">
        <div class="col-md-8">
            <div class="tab-content bg-silver padding-bg-product mr-3" id="nav-tabContent">
                <?php $i = 0 ?>
                @foreach($data as $row)
                    <div class="tab-pane fade show <?php if ($i == 0) {
                        echo "active";
                    } ?>" id="nav-{{$row->id}}" role="tabpanel" aria-labelledby="nav-home-tab">
                        @foreach($row->getProductTypeForSaleTab as $productType)
                            <div class="card-product">
                                <h5>{{$productType->name}}</h5>
                                <div class="row">
                                    @if($productType->parties == 0)
                                        @foreach($productType->getProductForProductType as $product)
                                            <div class="col-md-3 mb-2">
                                                <form method="post" action="{{route('addSaleTemp')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{$product->name}}" name="item">
                                                    <input type="hidden" value="{{$product->price}}" name="price">
                                                    <input type="hidden" value="{{$product->tax}}" name="tax">
                                                    <input type="hidden" value="{{$product->point}}" name="point">
                                                    <input type="hidden" value="{{$id}}" name="customer_id">
                                                    <input type="hidden" value=" {{ Auth::user()->id }}"
                                                           name="users_id">
                                                    <button type="submit"
                                                            class="btn product-title bg-{{$productType->button_colour}}"
                                                            data-toggle="modal" data-target="#Modal{{$product->id}}"
                                                            title="{{$product->price}}">
                                                        {{$product->name}}
                                                    </button>
                                                </form>
                                            </div>
                                        @endforeach
                                    @else
                                        @foreach($productType->getPartyProductForProductType as $product)
                                            <div class="col-md-4">
                                                <!-- Modal -->
                                                <button class="btn product-title bg-{{$productType->button_colour}}"
                                                        title="{{$product->description}}">
                                                    {{$product->name}}
                                                </button>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <?php $i++;?>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="bg-silver p-20">

                <a href="{{route('cashDrawer')}}" class="btn btn-cash bg-blue">CASH DRAWER</a>

                <?php if ($id != NULL || $id != '') {
                    $customer_data = \App\Customer::getCustomer($id);
                }?>
                <?php
                $temp = \App\Temp::getTempSale($id);
                ?>
                <?php if($id != NULL || $id != ''){?>
                <h5> Customer:
                    <a href="{{route('viewCustomer',$customer_data->id)}}">{{$customer_data->first_name}} {{$customer_data->last_name}}</a>
                </h5>
                <?php } ?>
                <hr>

                @if(count($temp)>0)
                    <h6>Items</h6>
                    <table class="item-temp">
                        <tbody>
                        <?php $t_point = 0 ?>
                        @foreach($temp as $temp_data)
                            <tr>
                                <input type="hidden" class="id_item" value="{{$temp_data->id}}">
                                <td width="45%" class="hover-pointer name_item pl-2" data-toggle="modal"
                                    data-target="#ModalQuantity">
                                    <span class="quantity_n_modal"
                                          style="font-size: 16px">{{$temp_data->quantity}}</span>
                                    {{$temp_data->item}}
                                    <?php $t_point += $temp_data->point;?>
                                </td>
                                <td width="14%">
                                    <?php $price_items = $temp_data->price * $temp_data->quantity;
                                    $price_items = sprintf('%0.2f', $price_items);?>
                                    {{$price_items}}
                                </td>
                                <td width="32%" class="text-center discount">
                                    <?php $discount_a = $temp_data->discount;?>
                                    @if($discount_a < 1 && $discount_a != 0 && $discount_a > -1)
                                        <?php $total_discount = $price_items - ($price_items * $discount_a); ?>
                                        = {{$total_discount = sprintf('%0.2f', $total_discount)}}
                                        (%{{abs($discount_a) * 100}} off)
                                    @elseif($discount_a == -1)
                                        <?php $total_discount = $price_items - $price_items; ?>
                                        = {{$total_discount = sprintf('%0.2f', $total_discount)}}
                                        (%{{abs($discount_a) * 100}} off)
                                    @elseif($discount_a == 0)
                                        {{' '}}
                                    @else
                                        <?php $total_discount = $price_items - $discount_a ?>
                                        =   {{$total_discount = sprintf('%0.2f', $total_discount)}} ( JD {{$discount_a}}
                                        off  )
                                    @endif

                                </td>
                                <td width="9%" class="text-center"><a
                                            href="{{route('deleteSaleTemp',$temp_data->id)}}"><i
                                                class="icon-close2"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <table class="mt-3 total-price">
                        <tbody>
                        <tr>
                            <td width="30%"></td>
                            <td width="20%">SubTotal:</td>
                            <td width="20%" class="text-center">
                                @if(count($temp) > 0)
                                    JD   {{$final_subtotal}}
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="pr-4">
                                <button class="btn btn-add bg-blue" data-toggle="modal" data-target="#ModalDiscount">
                                    Discount
                                </button>
                            </td>
                            <td width="20%">Tax:</td>
                            <td width="20%" class="text-center">
                                @if(count($temp) > 0)
                                    JD    {{$final_taxtotal}}
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <td width="30%"></td>
                            <td width="20%">Total:</td>
                            <td width="20%" class="text-center ">
                                @if(count($temp) > 0)
                                    JD     <span class="total">{{$total}}</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <div class="row">
                        <div class="col-12">
                            <button class="btn bg-blue btn-deposit w">Deposit</button>
                        </div>
                    </div>
                @endif
                <div class="payment-hide">
                    <div class="row ">
                        @foreach(\App\Payment::getPayment() as $payment)

                            <div class="col-6">
                                @if($payment->payment_type == "Cash")
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name" data-toggle="modal"
                                            data-target="#ModalPaymentCash">{{$payment->payment_name}}</button>
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name cash_val"
                                            data-toggle="modal"
                                            data-target="#ModalPaymentCash2">5 JD
                                    </button>

                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name cash_val"
                                            data-toggle="modal"
                                            data-target="#ModalPaymentCash2">20 JD
                                    </button>
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name cash_val"
                                            data-toggle="modal"
                                            data-target="#ModalPaymentCash2">50 JD
                                    </button>

                                @else
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name" data-toggle="modal"
                                            data-target="#ModalPayment">{{$payment->payment_name}}</button>
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name cash_val"
                                            data-toggle="modal"
                                            data-target="#ModalPaymentCash2">10 JD
                                    </button>
                                    <button class="btn bg-blue btn-deposit w-100 mt-3 payment_name cash_val"
                                            data-toggle="modal"
                                            data-target="#ModalPaymentCash2">30 JD
                                    </button>
                                @endif
                            </div>


                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modal')
    {{--Discount Modal--}}
    <div class="modal fade" id="ModalDiscount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLabel">Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('addDiscountItem')}}" method="post" onSubmit="return checkForm();">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="form-group">
                            <h6>Items</h6>
                            <?php
                            $temps_modals = \App\Temp::getTempSale($id);
                            ?>
                            @foreach($temps_modals as $item_model)
                                <div class="form-group">
                                    <input type="checkbox"
                                           @if(count($temps_modals) == 1) {{'checked'}} @endif class="mr-3"
                                           name="item[]" value="{{$item_model->id}}">
                                    <label>
                                        {{$item_model->item}}
                                    </label>
                                    <span class="dis-modal">
                                        @if($item_model->discount < 1 && $item_model->discount != 0)
                                            Currently Discounted: {{abs($item_model->discount) * 100}}%
                                        @elseif($item_model->discount == 0)
                                            {{' '}}
                                        @else
                                            Currently Discounted: {{$item_model->discount}} JD
                                        @endif
                                        {{$item_model->discount_reasons}}
                                    </span>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label for="discount">Discount</label>
                            <select name="discount" id="discount" class="form-control">
                                <option value="0">0</option>
                                @foreach(\App\Discount::getDiscount() as $discount)
                                    @if($discount->amount < 1)
                                        <?php $discount_modal = $discount->amount * 100 ?>
                                        <option value="{{$discount->amount}}">{{preg_replace('/-/','',$discount_modal)}}
                                            %
                                        </option>
                                    @else
                                        <?php $discount_modal = $discount->amount?>
                                        <option value="{{$discount->amount}}">{{$discount_modal}} JD</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="discount_reasons">Discount Reasons</label>
                            <select name="discount_reasons" id="discount_reasons" class="form-control">
                                <option value="" selected>Select Discount Reason</option>
                                @foreach(\App\DiscountReason::getDiscountReasons() as $reason)
                                    <option value="{{$reason->reason}}">{{$reason->reason}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="error-a" id="DiscountReasonError">Discount Reason is Required</div>
                        <div class="modal-footer2 text-center">
                            <button type="submit" class="btn  bg-blue">Add</button>
                            <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


    {{--quantity Modal--}}
    <div class="modal fade" id="ModalQuantity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title title_item"
                        id="exampleModalLabel">@if(!empty($product->name)) {{$product->name}} @endif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('addQuantityItem')}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <input type="hidden" name="id_item_modal" class="id_item_modal">
                        <div class="form-group">
                            <label for="quantity_modal">Quantity</label>
                            <input type="number" value="1" id="quantity_modal" class="form-control"
                                   name="quantity_modal">
                        </div>
                        <div class="modal-footer2 text-left">
                            <button type="submit" class="btn  bg-blue">Add</button>
                            <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @if(count($temp)>0)
        @foreach(\App\Payment::getPayment() as $payment_modal)
            {{--Payment--}}
            @if($payment_modal->payment_type == "Cash")
                <div class="modal fade" id="ModalPaymentCash" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="title_item_payment" id="exampleModalPayment"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body payment-modal text-center">
                                <h4 class="text-center">Payment type details</h4>
                                <form action="{{route('depositSale')}}" method="post" onSubmit="return checkPrice2();">
                                    {{csrf_field()}}
                                    <input type="hidden" name="sub_total"
                                           value="@if(count($temp) > 0){{$final_subtotal}}@endif">

                                    <input type="hidden" name="id_customer" value="{{$id}}">
                                    <input type="hidden" name="payment_m" class="paymenttype" value="Cash">
                                    <input type="hidden" name="taxtotal"
                                           value="@if(count($temp) > 0){{$final_taxtotal}}@endif">
                                    <input type="hidden" id="s_total2" name="s_total"
                                           value="@if(count($temp) > 0){{$total}}@endif">
                                    <?php  $temps_payment = \App\Temp::getTempSale($id); ?>
                                    @foreach($temps_payment as  $temp_modal)
                                        <input type="hidden" name="payment_id_item[]" value="{{$temp_modal->id}}">
                                    @endforeach
                                    @if($id != NULL || $id != '')
                                        @if(count($customer_data->getChildCustomer) > 0)
                                            <div class="child-model">
                                                <p>Select Child:</p>
                                                @foreach($customer_data->getChildCustomer as $child_modal)
                                                    <input type="checkbox" id="child-{{$child_modal->id}}"
                                                           name="child[]"
                                                           value="{{$child_modal->name}}">
                                                    <label for="child-{{$child_modal->id}}">
                                                        {{$child_modal->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif

                                    <h5>Total Sale: <span class="sale_total color-blue"></span> <span
                                                class="color-blue"> JD </span></h5>
                                    <h5><span style="color: red">
                                            Change Due: JD
                                        </span>
                                        <span class="due2" style="color: red"></span>
                                        (JD <span class="cash_res2"></span> - JD <span class="total-due2"></span>) </h5>
                                    <h5>Payment Date:
                                        <span class="color-blue">
                                <?php
                                            echo date('d-m-Y');
                                            ?>
                                </span></h5>
                                    <h5>Payment Time:
                                        <span class="color-blue">
                                <?php
                                            echo date('H:i:s');
                                            ?>
                                </span></h5>
                                    @if($id != NULL || $id != '')
                                        <div class="form-group mt-3">
                                            <div class="row">
                                                <label for="total_point" class="col-4">Visits</label>
                                                <?php
                                                $point_c = $customer_data->point;
                                                $all_point = $t_point;
                                                $all_point = $all_point / 10;?>
                                                <select id="total_point" name="total_point"
                                                        class="form-control col-6 select_visit">
                                                    <option value="{{$t_point}}">Choose Number Of Visits</option>
                                                    @if($t_point < 10)
                                                        <option value="{{$t_point}}"></option>
                                                    @endif
                                                    @for($i = 1; $i<=$all_point; $i++)
                                                        <option value="{{$t_point-($i*10)}}">{{$i}} @if($i == 1) {{"Visit"}} @else
                                                                Visits @endif (2 Hours)
                                                        </option>
                                                    @endfor
                                                    <input type="hidden" value="{{$t_point}}" name="t_point">
                                                </select>
                                                {{--<input type="hidden" class="ss_point" value="{{$t_point + $point_c}}">--}}
                                                {{--<input type="hidden" name="total_point" class="total_point">--}}
                                                <div class="col-2"></div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="cash_received" class="col-4">Cash Received</label>
                                            <input type="text" id="cash_received2" name="cash_received"
                                                   class="form-control col-6 sale_total_cash">
                                            <div class="col-2"></div>
                                        </div>
                                    </div>
                                    {{--<div class="form-group">--}}
                                    {{--<div class="row">--}}
                                    {{--<label for="sale_notes" class="col-4">Sale Notes</label>--}}
                                    {{--<textarea name="sale_notes" id="sale_notes"--}}
                                    {{--class="form-control col-6"></textarea>--}}
                                    {{--<div class="col-2"></div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="error-a" id="DiscountReasonError3">You must add more
                                        than @if(count($temp) > 0){{$total}}@endif</div>
                                    <div class="modal-footer2 text-left text-center">
                                        <button type="submit" class="btn  bg-blue">Deposit</button>
                                        <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal fade" id="ModalPaymentCash2" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="" id="exampleModalPayment">Cash</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body payment-modal text-center">
                                <h4 class="text-center">Payment type details</h4>
                                <form action="{{route('depositSale')}}" method="post" onSubmit="return checkPrice();">
                                    {{csrf_field()}}
                                    <input type="hidden" name="sub_total"
                                           value="@if(count($temp) > 0){{$final_subtotal}}@endif">

                                    <input type="hidden" name="id_customer" value="{{$id}}">
                                    <input type="hidden" name="payment_m" value="Cash">
                                    <input type="hidden" name="taxtotal"
                                           value="@if(count($temp) > 0){{$final_taxtotal}}@endif">
                                    <input type="hidden" id="s_total" name="s_total"
                                           value="@if(count($temp) > 0){{$total}}@endif">
                                    <?php  $temps_payment = \App\Temp::getTempSale($id); ?>
                                    @foreach($temps_payment as  $temp_modal)
                                        <input type="hidden" name="payment_id_item[]" value="{{$temp_modal->id}}">
                                    @endforeach
                                    @if($id != NULL || $id != '')
                                        @if(count($customer_data->getChildCustomer) > 0)
                                            <div class="child-model">
                                                <p>Select Child:</p>
                                                @foreach($customer_data->getChildCustomer as $child_modal)
                                                    <input type="checkbox" id="child-{{$child_modal->id}}"
                                                           name="child[]"
                                                           value="{{$child_modal->name}}">
                                                    <label for="child-{{$child_modal->id}}">
                                                        {{$child_modal->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif

                                    <h5>Total Sale: <span class="sale_total color-blue"></span> <span
                                                class="color-blue"> JD </span></h5>
                                    <h5><span style="color: red">
                                            Change Due: JD
                                        </span>
                                        <span class="due" style="color: red"></span>
                                        (JD <span class="cash_res"></span> - JD <span class="total-due"></span>) </h5>
                                    <h5>Payment Date:
                                        <span class="color-blue">
                                <?php
                                            echo date('d-m-Y');
                                            ?>
                                </span></h5>
                                    <h5>Payment Time:
                                        <span class="color-blue">
                                <?php
                                            echo date('H:i:s');
                                            ?>
                                </span></h5>
                                    @if($id != NULL || $id != '')
                                        <div class="form-group mt-3">
                                            <div class="row">
                                                <label for="total_point" class="col-4">Visits</label>
                                                <?php
                                                $point_c = $customer_data->point;
                                                $all_point = $t_point;
                                                $all_point = $all_point / 10;?>
                                                <select id="total_point" name="total_point"
                                                        class="form-control col-6 select_visit">
                                                    <option value="{{$t_point}}">Choose Number Of Visits</option>
                                                    @if($t_point < 10)
                                                        <option value="{{$t_point}}"></option>
                                                    @endif
                                                    @for($i = 1; $i<=$all_point; $i++)
                                                        <option value="{{$t_point-($i*10)}}">{{$i}} @if($i == 1) {{"Visit"}} @else
                                                                Visits @endif (2 Hours)
                                                        </option>
                                                    @endfor
                                                </select>
                                                {{--<input type="hidden" class="ss_point" value="{{$t_point + $point_c}}">--}}
                                                {{--<input type="hidden" name="total_point" class="total_point">--}}
                                                <div class="col-2"></div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="cash_received" class="col-4">Cash Received</label>
                                            <input type="text" id="cash_received" name="cash_received"
                                                   class="form-control cash_received col-6" value="0">
                                            <div class="col-2"></div>
                                        </div>
                                    </div>
                                    {{--<div class="form-group">--}}
                                    {{--<div class="row">--}}
                                    {{--<label for="sale_notes" class="col-4">Sale Notes</label>--}}
                                    {{--<textarea name="sale_notes" id="sale_notes"--}}
                                    {{--class="form-control col-6"></textarea>--}}
                                    {{--<div class="col-2"></div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="error-a" id="DiscountReasonError2">You must add more
                                        than @if(count($temp) > 0){{$total}}@endif</div>
                                    <div class="modal-footer2 text-left text-center">
                                        <button type="submit" class="btn  bg-blue">Deposit</button>
                                        <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            @else
                <div class="modal fade" id="ModalPayment" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="title_item_payment" id="exampleModalPayment"></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body payment-modal text-center">
                                <h4 class="text-center">Payment type details</h4>
                                <form action="{{route('depositSale')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="sub_total"
                                           value="@if(count($temp) > 0){{$final_subtotal}}@endif">
                                    <input type="hidden" name="id_customer" value="{{$id}}">
                                    <input type="hidden" name="payment_m" class="payment-type" value="">
                                    <input type="hidden" name="taxtotal"
                                           value="@if(count($temp) > 0){{$final_taxtotal}}@endif">
                                    <input type="hidden" name="s_total" value="@if(count($temp) > 0){{$total}}@endif">
                                    <?php  $temps_payment = \App\Temp::getTempSale($id); ?>
                                    @foreach($temps_payment as  $temp_modal)
                                        <input type="hidden" name="payment_id_item[]" value="{{$temp_modal->id}}">
                                    @endforeach
                                    @if ($id != NULL || $id != '')
                                        @if(count($customer_data->getChildCustomer) > 0)
                                            <div class="child-model">
                                                <p>Select Child:</p>
                                                @foreach($customer_data->getChildCustomer as $child_modal)
                                                    <input type="checkbox" id="child-{{$child_modal->id}}"
                                                           name="child[]"
                                                           value="{{$child_modal->name}}">
                                                    <label for="child-{{$child_modal->id}}">
                                                        {{$child_modal->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                    <h5>Total Sale: <span class="sale_total color-blue"></span> <span
                                                class="color-blue"> JD </span></h5>
                                    <h5>Payment Date:
                                        <span class="color-blue">
                                <?php
                                            echo date('d-m-Y');
                                            ?>
                                </span></h5>
                                    <h5>Payment Time:
                                        <span class="color-blue">
                                <?php
                                            echo date('H:i:s');
                                            ?>
                                </span></h5>
                                    @if ($id != NULL || $id != '')
                                        <div class="form-group mt-3">
                                            <div class="row">
                                                <label for="total_point" class="col-4">Visits</label>
                                                @php
                                                    $point_c = $customer_data->point;
                                                    $all_point = $t_point;
                                                    $all_point = $all_point / 10;
                                                @endphp

                                                {{--@if($t_point < 10)--}}
                                                {{--<input type="hidden" name="add_point" value="{{$t_point}}">--}}
                                                {{--@endif--}}
                                                <select id="total_point" name="total_point"
                                                        class="form-control col-6 select_visit">
                                                    <option value="{{$t_point}}">Choose Number Of Visits</option>
                                                    @if($t_point < 10)
                                                        <option value="{{$t_point}}"></option>
                                                    @endif
                                                    @for($i = 1; $i<=$all_point; $i++)
                                                        <option value="{{$t_point-($i*10)}}">{{$i}} @if($i == 1) {{"Visit"}} @else
                                                                Visits @endif (2 Hours)
                                                        </option>
                                                    @endfor
                                                </select>
                                                {{--<input type="hidden" class="ss_point" value="{{$t_point + $point_c}}">--}}
                                                {{--<input type="hidden" name="total_point" class="total_point">--}}
                                                <div class="col-2"></div>
                                            </div>
                                        </div>
                                    @endif
                                    {{--<div class="form-group">--}}
                                    {{--<div class="row">--}}
                                    {{--<label for="sale_notes" class="col-4">Sale Notes</label>--}}
                                    {{--<textarea name="sale_notes" id="sale_notes"--}}
                                    {{--class="form-control col-6"></textarea>--}}
                                    {{--<div class="col-2"></div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="modal-footer2 text-left text-center">
                                        <button type="submit" class="btn  bg-blue">Deposit</button>
                                        <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
@endsection
@section('page_js')


    <script>
        $('.btn-deposit').on('click', function () {
            $('.payment-hide').fadeToggle(300);
        });
        var quantity_update = function () {
            $('.name_item').on('click', function () {
                var name_item = $(this).text();
                var quantity_n_modal = $(this).parent().find('.quantity_n_modal').text();
                var id_item_modal = $(this).parent().find('.id_item').val();
                $('.title_item').text(name_item);
                $('#quantity').val(quantity_n_modal)
                $('.id_item_modal').val(id_item_modal);
            });
        }
        var payment = function () {
            $('.payment_name').on('click', function () {
                var payment_name = $(this).text();
                var Sale_Total = $('.total').text();

                $('.title_item_payment').text(payment_name);
                $('.sale_total').text(Sale_Total);
                $('.total-due').text(Sale_Total);
                $('.payment-type').val(payment_name);
                $('.sale_total_cash').val(parseInt(Sale_Total));

            });
        }
        var cash_val = function () {
            $('.cash_val').on('click', function () {
                var Sale_Total2 = $('.total').text();
                var text_val = $(this).text();
                $('.cash_received').val(parseInt(text_val));
                var n = parseInt(text_val);
                var l = n.toFixed(2);
                $('.cash_res').text(l);
                var due = l - Sale_Total2;
                var o = due.toFixed(2);
                $('.due').text(o);
            });
        }
        var sum_due = function () {
            var Sale_Total3 = $('.total').text();
            var cash_res3 = $('.cash_res2').text(Sale_Total3);
            var text_cash_res3 = $('.cash_res2').text()
            var total_due2 = $('.total-due2').text(Sale_Total3);
            var s_due = parseInt(text_cash_res3) - parseInt(Sale_Total3);
            $('.due2').text(s_due);
            $('#cash_received2').on('keyup',function () {
                var valThis = $(this).val();
                var c = parseInt(valThis);
                var d = c.toFixed(2);
                $('.cash_res2').text(d);
                var x = parseInt(c) -parseInt(Sale_Total3);
                var l = x.toFixed(2);
                $('.due2').text(l);
            })
        };
        quantity_update();
        payment();
        cash_val();
        sum_due();


        function checkForm() {
            discount = document.getElementById("discount").value;
            discountreason = document.getElementById("discount_reasons").value;

            if (discountreason == "" && discount != "") {
                hideAllErrors();
                document.getElementById("DiscountReasonError").style.display = "block";
                return false;
            }

            return true;
        }

        function hideAllErrors() {
            document.getElementById("DiscountReasonError").style.display = "none";
        }

        function checkPrice() {
            s_total = document.getElementById("s_total").value;
            cash_received = document.getElementById("cash_received").value;

            if (parseInt(cash_received) < parseInt(s_total)) {
                hideAllErrors2();
                document.getElementById("DiscountReasonError2").style.display = "block";
                return false;
            }
            return true;

        }

        function hideAllErrors2() {
            document.getElementById("DiscountReasonError2").style.display = "none";
        }

        function checkPrice2() {
            s_total2 = document.getElementById("s_total2").value;
            cash_received2 = document.getElementById("cash_received2").value;

            if (parseInt(cash_received2) < parseInt(s_total2)) {
                hideAllErrors2();
                document.getElementById("DiscountReasonError3").style.display = "block";
                return false;
            }
            return true;

        }

        function hideAllErrors3() {
            document.getElementById("DiscountReasonError3").style.display = "none";
        }

        $(function () {
            $('a[data-toggle="tab"]').on('click', function (e) {
                window.localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = window.localStorage.getItem('activeTab');
            if (activeTab) {
                $('#nav-tab a[href="' + activeTab + '"]').tab('show');
                // window.localStorage.removeItem("activeTab");
            }
        });
    </script>
@endsection