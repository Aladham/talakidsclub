@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Deleted Customers</h3>
        </div>
        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone 1</th>
                    <th scope="col">Phone 2</th>
                    <th scope="col">Notes</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td><a href="#">Create Sale</a></td>
                        <td>
                            <a href="{{route('viewCustomer',$row->id)}}" class="c-view">
                                {{$row->first_name}} {{$row->last_name}}
                            </a>
                        </td>
                        <td>{{$row->primary_phone}}</td>
                        <td>{{$row->secondary_phone}}</td>
                        <td>{{$row->notes}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('activateCustomer',$row->id)}}" class="btn bg-blue">Activate</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

@endsection