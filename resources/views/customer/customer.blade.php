@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="row">
            <div class="col-sm-2 mb-4">
                <a href="{{route('deleteAllSaleTemp',$data->id)}}" class="btn bg-blue btn-add">
                    Make Order
                </a>
            </div>
            <div class="col-sm-2 mb-4">
                <button class="btn bg-blue btn-add" data-toggle="modal"
                        data-target="#ModalPoint">Use Points
                </button>
            </div>
            <div class="col-sm-2 mb-4">
                <a href="{{route('ViewAddChild',$data->id)}}" class="btn bg-blue btn-add">Add Child</a>
            </div>
            <div class="col-sm-2 mb-4">
                <a href="{{route('editCustomer',$data->id)}}" class="btn bg-blue btn-add">Edit</a>
            </div>
            <div class="col-sm-2 mb-4">
                <a href="{{route('customers')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <p><strong class="w-120 d-inline-block">Customer
                        Name:</strong> {{$data->first_name}} {{$data->last_name}} </p>
                <p><strong class="w-120 d-inline-block">Customer ID:</strong> {{$data->id}} &nbsp; &nbsp;(Date
                    Created: {{$data->created_at->format('Y-m-d')}}) </p>
                <p><strong class="w-120 d-inline-block">Primary Phone:</strong> {{$data->primary_phone}} </p>
                <p><strong class="w-120 d-inline-block">Secondary Phone:</strong> {{$data->secondary_phone}} </p>
                <p><strong class="w-120 d-inline-block">Points:</strong> {{$data->point}} </p>
                <p><strong class="w-120 d-inline-block">Notes:</strong> {{$data->notes}} </p>
            </div>
        </div>

        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('message')}}</span>
            </div>
        @endif

        @if(session()->has('Errormessage'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('Errormessage')}}</span>
            </div>
        @endif
        @if(count($data_child) > 0)
            <h3 class="mt-4">Children</h3>
            <div class="table-responsive">
                <table id="customers_table" class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th class="text-center" scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php $i = 1; ?>
                    @foreach( $data_child as $row)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->gender}}</td>
                            <td class="text-center action-btn">
                                <a href="{{route('editChild',$row->id)}}" class="btn bg-blue">Edit</a>
                                <a href="{{route('deleteChild',$row->id)}}" onclick="confirmDelete()"
                                   class="btn bg-red">Delete</a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach

                    </tbody>
                </table>
            </div>
        @endif




        @if(count($data_sales) > 0)
            <h3 class="mt-4">Sales</h3>
            @if (Auth::user()->type == 0) <a href="{{route('deleteAllSaleCustomer',$data->id)}}"
                                             class="btn btn-danger bg-red">Delete All</a> @endif
            @foreach( $data_sales as $sale)
                <div class="table-responsive">
                    <table id="customers_table" border="1" class="mt-3" style="width: 70%">
                        <thead class="thead-light">
                        <tr>
                            @if (Auth::user()->type == 0)
                                <th></th>
                            @endif
                            <th scope="col">Sale ID</th>
                            <th scope="col">Total</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col">Payment Type</th>
                            <th scope="col">Employee</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @if (Auth::user()->type == 0)
                                <th><a href="{{route('deleteSaleCustomer',$sale->id)}}" onclick="confirmDelete()"
                                       style="color: red"><i class="icon-close2"></i></a></th>

                                {{--<td class="text-center underline-a">--}}
                                    {{--<a href="{{route('DataSale',$sale->id)}}">--}}
                                        {{--{{$sale->id}}--}}
                                    {{--</a>--}}
                                {{--</td>--}}
                            @endif
                            <td class="text-center">{{$sale->order_s}}</td>
                            <td>{{$sale->total}} JD</td>
                            <td>{{$sale->created_at->format((' F j, Y'))}}
                                @ {{$sale->created_at->format((' g:i:s a'))}}</td>
                            <td>{{$sale->payment}}</td>
                            <td>{{$sale->getEmpForSale->username}}</td>
                        </tr>
                        @foreach($sale->getProductForSale as $product_sale)
                            <tr>
                                <td></td>
                                <td colspan="6">{{$product_sale->item}}</td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            @endforeach
        @endif
        @php
            $id = $data->id;
                $data_visits = \App\Customer::getVisits($id);
        @endphp

        <h3 class="mt-5">Visits</h3>
        <div class="visit-t">
            <table id="customers_table" border="1" class="mt-3 visit_t" style="width: 70%;">
                <thead class="thead-light">
                <tr align="center">
                    <th scope="col">Number Of Visits</th>
                    <th scope="col">Date & Time</th>
                    @if(Auth::user()->type == 0)
                        <td>Action</td>  @endif
                </tr>
                </thead>
                <tbody>
                @php $j = 0; @endphp
                @foreach( $data_visits as $visit)
                    <tr align="center">
                        <td>{{$visit->points}}</td>
                        <td>{{$visit->created_at->format(' F j, Y')}} @ {{$visit->created_at->format(' g:i a')}}</td>
                        @if(Auth::user()->type == 0)
                            <td>
                                <a href="{{route('deleteVisit',$visit->id)}}" style="color: #fff"
                                   onclick="confirmDelete()"
                                   class="btn bg-red ml-1">Delete</a>
                            </td>
                        @endif
                    </tr>
                    @php $j++ @endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('modal')
    <div class="modal fade" id="ModalPoint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title_item_payment" id="exampleModalPayment"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body payment-modal text-center">
                    <h4 class="text-center">Use Points</h4>
                    <form action="{{route('usePoint')}}" method="post" class="mt-4">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <input type="hidden" name="id_c" value="{{$data->id}}">
                        <div class="child-model mb-3">
                            @if(count($data_child) > 0)
                                <p>Select Child:</p>
                                @foreach($data_child as $child_modal)
                                    <input type="checkbox" id="child-{{$child_modal->id}}" name="child[]"
                                           value="{{$child_modal->name}}">
                                    <label for="child-{{$child_modal->id}}">
                                        {{$child_modal->name}}
                                    </label>
                                @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
                                {{--<label for="point" class="col-4">Points ( {{$data->point}} )</label>--}}
                                {{--<input type="text" id="point" name="point" class="form-control col-6 total-point"--}}
                                {{--value="0" placeholder="Number of points to be used">--}}
                                {{--<div class="col-2"></div>--}}
                                <label for="total_point" class="col-4">Visits</label>
                                <?php
                                $all_point = $data->point / 10;?>
                                <select id="total_point" name="total_point" class="form-control col-6 select_visit">
                                    @for($i = 1; $i<=$all_point; $i++)
                                        <option value="{{$data->point-($i*10)}}">{{$i}} @if($i == 1) {{"Visit"}} @else
                                                Visits @endif (2 Hours)</option>
                                    @endfor
                                </select>
                                <div class="col-2"></div>
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                        {{--<div class="row">--}}
                        {{--<label for="free_hours" class="col-4">Free Hours</label>--}}
                        {{--<input type="text" id="free_hours" name="free_hours" class="form-control col-6">--}}
                        {{--<div class="col-2"></div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                        {{--<div class="row">--}}
                        {{--<label for="notes" class="col-4">Notes</label>--}}
                        {{--<textarea name="notes" id="notes" class="form-control col-6 note-model2"></textarea>--}}
                        {{--<div class="col-2"></div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="error-a" id="PointError">The number of points must be less than <span
                                    class="num-point"></span></div>
                        <div class="modal-footer2 text-left text-center">
                            <button type="submit" class="btn  bg-blue">Use</button>
                            <button type="button" class="btn bg-red" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }

        $('.visit_t').dataTable({
            "searching": false
        });
    </script>.

@endsection