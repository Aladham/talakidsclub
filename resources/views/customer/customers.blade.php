@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Customers</h3>

        </div>
        <div class="row">
            <div class="col-sm-3">
                <a href="{{route('ViewAddCustomer')}}" class="btn bg-blue btn-add"> Add New Customer</a>
            </div>
            <div class="col-sm-4">
                <div class="bg-silver p-20 text-center mb-3">
                    <h6>Customer Search</h6>
                    <form action="{{route('searchCustomers')}}">
                        <div class="form-group">
                            <input type="text" name="data" class="form-control input-search"
                                   placeholder="Name or Phone or #ID">
                        </div>
                        <button class="btn bg-blue btn-add" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('message')}}</span>
            </div>
        @endif
        @if(session()->has('error_message'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('error_message')}}</span>
            </div>
        @endif
        <div class="table-responsive">
            <table id="customers_table" style="width: 100%;    border-collapse: separate;">
                <thead class="thead-light">
                <tr>

                    <th scope="col">#</th>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone 1</th>
                    <th scope="col">Phone 2</th>
                    <th scope="col">Points</th>
                    <th scope="col">Notes</th>
                    <th class="text-center" scope="col">Action</th>

                </tr>
                </thead>
                <tbody>

                <?php $i = ($data->currentpage()-1)* $data->perpage() + 1;?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td><a href="{{route('deleteAllSaleTemp',$row->id)}}" class="c-view">Create Sale</a></td>
                        <td>
                            <a href="{{route('viewCustomer',$row->id)}}" class="c-view">
                                {{$row->first_name}} {{$row->last_name}}
                            </a>
                        </td>
                        <td>{{$row->primary_phone}}</td>
                        <td>{{$row->secondary_phone}}</td>
                        <td class="td-point">{{$row->point}}</td>
                        <td class="td-point" width="220">{{$row->notes}}</td>
                        <td class="text-center action-btn">
                            <input type="hidden" class="id-modal" value="{{$row->id}}">
                            <input type="hidden" class="note-modal" value="{{$row->notes}}">
                            <input type="hidden" class="hours-modal" value="{{$row->free_hours}}">

                            <a href="{{route('viewCustomer',$row->id)}}" class="btn bg-green ml-1">View</a>
                            <a href="{{route('editCustomer',$row->id)}}" class="btn bg-blue ml-1">Edit</a>
                            @if(Auth::user()->type == 0)
                                <a href="{{route('deleteCustomer',$row->id)}}" onclick="confirmDelete()"
                                   class="btn bg-red ml-1">Delete</a>
                            @endif
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach


                </tbody>
            </table>

            <div class="pagen-a mt-5">
                {{ $data->links() }}
            </div>



        </div>
        <div>Showing {{($data->currentpage()-1)*$data->perpage()+1}} to {{$data->currentpage()*$data->perpage()}}
            of  {{$data->total()}} entries
        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    @if(Auth::user()->type == 0)
        <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/js/jszip.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>
        <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    @endif
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        // var data_customer = function () {
        //     $('.btn-point').on('click', function () {
        //         var id = $(this).parent().find('.id-modal').val();
        //         var note = $(this).parent().find('.note-modal').val();
        //         var point = $(this).parent().parent().find('.td-point').text();
        //
        //         //
        //         $('.id_c').val(id);
        //         $('.note-model2').text(note);
        //         $('.span-point').text(point);
        //         $('.num-point').text(point);
        //         // $('.sale_total').text(Sale_Total);
        //         // $('.payment-type').val(payment_name);
        //
        //     });
        // }
        // data_customer();
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }

        {{--$(document).ready(function () {--}}
            {{--$('.table2').DataTable({--}}
                {{--"processing": true,--}}
                {{--"serverSide": true,--}}
                {{--"ajax": "{{route('getCustomersapi')}}",--}}
                {{--"columns":[--}}
                    {{--{"render":function(data, type, row){--}}
                        {{--return row["first_name"] + " " +  row["last_name"];--}}
                    {{--}},--}}
                    {{--{"data": "primary_phone"},--}}
                    {{--{"data": "secondary_phone"},--}}
                    {{--{"data": "point"},--}}
                    {{--{"data": "notes"},--}}
                {{--@if(Auth::user()->type == 0)--}}
              {{----}}
                {{--@endif--}}
                 {{----}}
                {{--]--}}

            {{--});--}}
        {{--});--}}
    </script>.

@endsection