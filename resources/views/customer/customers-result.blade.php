@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Customers Result</h3>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <a href="{{route('ViewAddCustomer')}}" class="btn bg-blue btn-add"> Add New Customer</a>
            </div>
            <div class="col-sm-4">
                <div class="bg-silver p-20 text-center">
                    <h6>Customer Search</h6>
                    <form action="{{route('searchCustomers')}}">
                        <div class="form-group">
                            <input type="text" name="data" class="form-control input-search"
                                   placeholder="Name or Phone or #ID">
                        </div>
                        <button class="btn bg-blue btn-add" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="table-responsive">
            <table id="customers_table" class="table" style="white-space: nowrap;">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone 1</th>
                    <th scope="col">Phone 2</th>
                    <th scope="col">Points</th>
                    <th scope="col">Notes</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td><a href="{{route('deleteAllSaleTemp',$row->id)}}">Create Sale</a></td>
                        <td>
                            <a href="{{route('viewCustomer',$row->id)}}" class="c-view">
                                {{$row->first_name}} {{$row->last_name}}
                            </a>
                        </td>
                        <td>{{$row->primary_phone}}</td>
                        <td>{{$row->secondary_phone}}</td>
                        <td class="td-point">{{$row->point}}</td>
                        <td class="td-point" width="220">{{$row->notes}}</td>
                        <td class="text-center action-btn">
                            <input type="hidden" class="id-modal" value="{{$row->id}}">
                            <input type="hidden" class="note-modal" value="{{$row->notes}}">
                            <input type="hidden" class="hours-modal" value="{{$row->free_hours}}">
                            <a href="{{route('viewCustomer',$row->id)}}" class="btn bg-purple ml-1 btn-point" data-toggle="modal"
                               data-target="#ModalPoint">Use Points</a>
                            <a href="{{route('viewCustomer',$row->id)}}" class="btn bg-green ml-1">View</a>
                            <a href="{{route('editCustomer',$row->id)}}" class="btn bg-blue ml-1">Edit</a>
                            <a href="{{route('deleteCustomer',$row->id)}}" onclick="confirmDelete()" class="btn bg-red ml-1">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>

        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection