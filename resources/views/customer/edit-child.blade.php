@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Edit Child</h3>
        </div>


        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('viewCustomer',$data->fk_parent_id)}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updateChild',$data->id)}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label for="gender" class="col-sm-4 col-form-label">Gender:</label>--}}
                        {{--<div class="col-sm-7">--}}
                            {{--<select name="gender" id="gender" class="form-control">--}}
                                {{--<option value="Male" @if($data->gender == "Male") {{'selected'}}  @endif>Male</option>--}}
                                {{--<option value="Female" @if($data->gender == "Female") {{'selected'}}  @endif>Female</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label for="gender" class="col-sm-4 col-form-label">Gender:</label>
                        <div class="col-sm-7">
                            <div class="row">
                                <div class="col-4">
                                    <input type="radio" id="male" name="gender" @if($data->gender == "Male") {{'checked'}}  @endif value="Male">
                                    <label for="male">Male</label>
                                </div>
                                <div class="col-4">
                                    <input type="radio" id="female" name="gender" value="Female" @if($data->gender == "Female") {{'checked'}}  @endif>
                                    <label for="female">Female</label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Child</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection
