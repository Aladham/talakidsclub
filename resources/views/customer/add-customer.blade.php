@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Add Customer</h3>
        </div>
        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('customers')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddCustomer')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="firstName" class="col-sm-4 col-form-label">First Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="firstName" name="firstName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastName" class="col-sm-4 col-form-label">Last Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="lastName" name="lastName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="primary_phone" class="col-sm-4 col-form-label">Primary Phone:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="primary_phone" name="primary_phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="secondaryPhone" class="col-sm-4 col-form-label">Secondary Phone:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="secondaryPhone" name="secondaryPhone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label">Notes:</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" id="notes" name="notes"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Customer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection

