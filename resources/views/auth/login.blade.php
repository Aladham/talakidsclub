<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tala Kids Club - login</title>
    <link rel="icon" href="{{asset('assets/login/img/favicon.png')}}" type="image/png" sizes="16x16">
    <link href="{{asset('assets/login/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/login/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/login/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/login/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/login/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/login/css/main.css')}}" rel="stylesheet" type="text/css">

</head>

<body class="login-container">

<!-- Main navbar -->

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Simple login form -->
                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="text-slate-300 logo">
                                <img src="{{asset('assets/login/img/logo.png')}}" >
                            </div>
                            <h5 class="content-group">Login: Please select Username, enter your password and click
                                Login.</h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('username') ? ' has-error' : '' }}">
                            <select name="username" class="form-control" >
                                <option>Select Username</option>
                                @foreach(\App\User::get_user() as $row)
                                    <option value="{{$row->username}}">{{$row->username}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('username'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" placeholder="Password"
                                   name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-login bg-pink-400 btn-block"><i
                                        class="icon-arrow-right14 position-right"></i>
                                Login
                            </button>
                        </div>

                    </div>
                </form>
                <!-- /simple login form -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
