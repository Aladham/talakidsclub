@extends('layouts.app')
@section('page_css')
    <link href="{{asset('assets/css/epoch_styles.css')}}" rel="stylesheet"/>

@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Reports</h3>
        </div>

        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

    </div>
    <hr>
    <div class="row mb-4">
        <div class="col-md-2"></div>
        <div class="col-md-8 ">
            <div class="card-report">
                <h5>1. Daily Summary Report</h5>
                <p>Shows overall summary of sales by product type, discounts, taxes, refunds and voids.
                </p>
                <form action="{{route('dailySummaryReport')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-2">
                            <label for="popup_container1">Start Date:</label>
                        </div>
                        <div class="col-md-6">
                         <input autocomplete="off" type="text" id="popup_container1" name="start_date" class="form-control" value="{{now()->format('Y-m-d')}}">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-2">
                            <label for="popup_container2">End Date:</label>
                        </div>
                        <div class="col-md-6">
                            <input autocomplete="off" type="text" id="popup_container2" name="end_date" class="form-control" value="{{now()->format('Y-m-d')}}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-add bg-blue mt-3 btn-report">View Report</button>
                </form>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-2"></div>
        <div class="col-md-8 ">
            <div class="card-report">
                <h5>2. Summary By Date</h5>
                <form action="{{route('summaryBey')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-2">
                            <label for="popup_container1">Start Date:</label>
                        </div>
                        <div class="col-md-6">
                            <input autocomplete="off" type="text" id="popup_container4" name="start_date" class="form-control" value="{{now()->format('Y-m-d')}}">
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-2">
                            <label for="popup_container2">End Date:</label>
                        </div>
                        <div class="col-md-6">
                            <input autocomplete="off" type="text" id="popup_container5" name="end_date" class="form-control" value="{{now()->format('Y-m-d')}}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-add bg-blue mt-3 btn-report">View Report</button>
                </form>
            </div>
        </div>
    </div>

    {{--<div class="row mb-4">--}}
        {{--<div class="col-2"></div>--}}
        {{--<div class="col-8">--}}
            {{--<div class="card-report">--}}
                {{--<h5>2. Sales Report By Product</h5>--}}
                {{--<p>Shows sales totals for each Product.</p>--}}
                {{--<form action="{{route('salesReportByProduct')}}" method="post">--}}
                    {{--{{csrf_field()}}--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-3">--}}
                            {{--<label for="popup_container4">Start Date:</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input autocomplete="off" type="text" id="popup_container4" name="start_date" class="form-control" value="{{now()->format('Y-m-d')}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="row mt-3">--}}
                        {{--<div class="col-sm-3">--}}
                            {{--<label for="popup_container5">End Date:</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<input autocomplete="off" type="text" id="popup_container5" name="end_date" class="form-control" value="{{now()->format('Y-m-d')}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row mt-3">--}}
                        {{--<div class="col-sm-3">--}}
                            {{--<label for="product">Product:</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<select name="product" id="product">--}}
                                {{--<option value="">Show All</option>--}}
                                {{--@php--}}
                                    {{--$products = \App\Product::getProduct();--}}
                                {{--@endphp--}}
                                {{--@foreach($products as $product)--}}
                                    {{--<option value="{{$product->name}}">{{$product->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row mt-3">--}}
                        {{--<div class="col-sm-3">--}}
                            {{--<label for="product_type">Product Type:</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<select name="product_type" id="product_type">--}}
                                {{--<option value="">Show All</option>--}}
                                {{--@php--}}
                                   {{--$product_type = \App\Product::getProductType();--}}
                                {{--@endphp--}}
                                {{--@foreach($product_type as $type)--}}
                                    {{--<option value="{{$type->name}}">{{$type->name}}</option>--}}
                                    {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<button type="submit" class="btn btn-add bg-blue mt-3 btn-report">View Report</button>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection


@section('page_js')
    <script src="{{asset('assets/js/epoch_classes.js')}}"></script>
    <script>
        var cal;
        window.onload = function () {
            cal = new Epoch('epoch_popup', 'popup', document.getElementById('popup_container1'));
            cal2  = new Epoch('epoch_popup','popup',document.getElementById('popup_container2'));
            cal3  = new Epoch('epoch_popup','popup',document.getElementById('popup_container4'));
            cal4  = new Epoch('epoch_popup','popup',document.getElementById('popup_container5'));
        }
    </script>
@endsection