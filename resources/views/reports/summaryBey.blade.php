@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Summary By Date Report</h3>
        </div>
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('reports')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

    </div>
    <hr>
    <?php
    $start_date = DateTime::createFromFormat('Y-m-d', $start_date);
    $end_date = DateTime::createFromFormat('Y-m-d', $end_date);
    ?>
    {{$start_date->format('l, F jS, Y')}} - {{$end_date->format('l, F jS, Y')}}
    <hr>
    @if(count($summaryAccount) > 0)
        <div class="row">
            <div class="col-md-8">
                <table id="SalesReportByProduct" class="table report-table table1" style="border-collapse: separate;">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Cash</th>
                        <th>Visa</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($summaryAccount as $re1)
                        <tr>
                            <td>{{$re1->Date}}</td>
                            <td>JD {{$re1->totalCash}}</td>
                            <td>JD {{$re1->totalVISA}}</td>
                            <td>JD {{$re1->totalCash + $re1->totalVISA}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @else
        <p>There are no sales during the selected period.</p>
    @endif
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    @if(Auth::user()->type == 0)
        <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/js/jszip.min.js')}}"></script>
        <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>
        <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    @endif
    <script src="{{asset('assets/js/main.js')}}"></script>
@endsection