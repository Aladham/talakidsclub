@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/tableexport.min.css')}}">

@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Daily Summary Report</h3>
        </div>

        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('reports')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

    </div>
    <hr>
    <?php
    $start_date = DateTime::createFromFormat('Y-m-d', $start_date);
    $end_date = DateTime::createFromFormat('Y-m-d', $end_date);
    ?>
    {{$start_date->format('l, F jS, Y')}} - {{$end_date->format('l, F jS, Y')}}
    <hr>
    <h5>Sales Report By Product</h5>
    @if(count($data) > 0)
        <div class="row">
            <div class="col-md-8">
                <table id="SalesReportByProduct" class="table report-table table1" style="border-collapse: separate;">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Sale Tab</th>
                        <th>Product Types</th>
                        <th>Quantity</th>
                        <th>Sales</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total_sale = 0;?>
                    @foreach($data as $re1)
                        <?php $sales = sprintf('%0.2f', $re1->Sales);
                        $total_sale+= $sales;
                        ?>
                        <tr>
                            <td>{{$re1->Product_Name}}</td>
                            <td>{{$re1->Sale_Tab}}</td>
                            <td>{{$re1->Product_Types}}</td>
                            <td>{{$re1->Quantity}}</td>
                            <td>JD {{$sales}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td>JD {{$total_sale}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-4">
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>

            </div>
        </div>
        <hr>
    @else
        <p>There are no sales during the selected period.</p>
    @endif
@endsection


@section('page_js')
    <script src="{{asset('assets/js/FileSaver.js')}}"></script>
    <script src="{{asset('assets/js/tableexport.min.js')}}"></script>

    <script>


        $(".table1").tableExport({
            headers: true,
            position: "top",
            formats: ["csv"],
            fileName: "newFile"

        });


    </script>
    <script src="{{asset('assets/js/canvasjs.min.js')}}"></script>

@endsection