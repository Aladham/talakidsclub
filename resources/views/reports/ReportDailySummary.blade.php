@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/tableexport.min.css')}}">

@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Daily Summary Report</h3>
        </div>

        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('reports')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

    </div>
    <hr>
    <?php
    $start_date = DateTime::createFromFormat('Y-m-d', $start_date);
    $end_date = DateTime::createFromFormat('Y-m-d', $end_date);
    ?>
    {{$start_date->format('l, F jS, Y')}} - {{$end_date->format('l, F jS, Y')}}
    <hr>
    <h5>Product Types Summary</h5>
    @if(count($product_Types_Summary)>0)
        <div class="row no-gutters">
            <div class="col-md-9">
                <div class="table-responsive">
                    <table id="ProductTypesSummary" class="table report-table table1"
                           style="border-collapse: separate;">
                        <thead>
                        <tr>
                            <th>Quantity</th>
                            <th>Sales Tab</th>
                            {{--<th>Product Types</th>--}}
                            <th>Product Type</th>
                            <th>Pre-Tax</th>
                            <th>Sales Tax (16%)</th>
                            <th>Total</th>
                            <th>Percentage</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total_quantity = 0;
                        $total_asPreTax = 0;
                        $total_SalesTax = 0;

                        ?>
                        @foreach($product_Types_Summary as $re1)
                            <?php

                            $quantity = $re1->Quantity;
                            $asPreTax_t = $re1->asPreTax;
                            $SalesTax_t = $re1->SalesTax;
                            $total_t = $re1->Total;

                            $total_quantity += $quantity;
                            $total_asPreTax += $asPreTax_t;
                            $total_SalesTax += $SalesTax_t;

                            ?>
                            <tr>
                                <?php
                                $asPreTax = sprintf('%0.2f', $re1->asPreTax);
                                $SalesTax = sprintf('%0.2f', $re1->SalesTax);
                                $Total = sprintf('%0.2f', $re1->Total);
                                $total_asPreTax = sprintf('%0.2f', $total_asPreTax);
                                $total_total = sprintf('%0.2f', $total_total);
                                $percentage = ($total_t / $total_total) * 100;
                                $percentage = round($percentage, 1);
                                ?>
                                <td>{{$re1->Quantity}}</td>
                                <td>{{$re1->Sale_Tab}}</td>
                                {{--<td>{{$re1->Product_Types}}</td>--}}
                                <td> @if($re1->Product_Types == "Extra Items") {{"Socks"}} @else{{$re1->Product_Types}}  @endif</td>
                                <td>JD {{$asPreTax}}</td>
                                <td>JD {{$SalesTax}}</td>
                                <td>JD {{$Total}}</td>
                                <td> {{$percentage}}%</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>{{$total_quantity}}</td>
                            <td colspan="2">Total</td>
                            <td>JD {{$total_asPreTax}}</td>
                            <td>JD {{sprintf('%0.2f', $total_SalesTax)}}</td>
                            <td>JD {{$total_total}}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <div id="piechart" style="max-width: 100%;min-width: 100%; height: 300px;"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8">
                <div class="table-responsive">
                    <table id="ProductTypesSummary" class="table report-table table1"
                           style="border-collapse: separate;">
                        <thead>
                        <tr>
                            <th>Quantity</th>
                            <th>Sales Tab</th>
                            {{--<th>Product Types</th>--}}
                            <th>Product</th>
                            <th>Pre-Tax</th>
                            <th>Sales Tax (16%)</th>
                            <th>Total</th>
                            <th>Percentage</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total_quantity = 0;
                        $total_asPreTax = 0;
                        $total_SalesTax = 0;

                        ?>
                        @foreach($products_Summary as $re8)
                            <?php

                            $quantity = $re8->Quantity;
                            $asPreTax_t = $re8->asPreTax;
                            $SalesTax_t = $re8->SalesTax;
                            $total_t2 = $re8->Total;

                            $total_quantity += $quantity;
                            $total_asPreTax += $asPreTax_t;
                            $total_SalesTax += $SalesTax_t;

                            ?>
                            <tr>
                                <?php
                                $asPreTax = sprintf('%0.2f', $re8->asPreTax);
                                $SalesTax = sprintf('%0.2f', $re8->SalesTax);
                                $Total = sprintf('%0.2f', $re8->Total);
                                $total_asPreTax = sprintf('%0.2f', $total_asPreTax);
                                $total_total2 = sprintf('%0.2f', $total_total2);
                                $percentage2 = ($total_t2 / $total_total2) * 100;
                                $percentage2 = round($percentage2, 1);
                                ?>
                                <td>{{$re8->Quantity}}</td>
                                <td>{{$re8->Sale_Tab}}</td>
                                {{--<td>{{$re1->Product_Types}}</td>--}}
                                <td>{{$re8->item}}</td>
                                <td>JD {{$asPreTax}}</td>
                                <td>JD {{$SalesTax}}</td>
                                <td>JD {{$Total}}</td>
                                <td> {{$percentage2}}%</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>{{$total_quantity}}</td>

                            <td colspan="2">Total</td>
                            <td>JD {{$total_asPreTax}}</td>
                            <td>JD {{sprintf('%0.2f', $total_SalesTax)}}</td>
                            <td>JD {{$total_total2}}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-md-4">
                <div id="piechart2" style="max-width: 100%;min-width: 100%; height: 300px;"></div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-4">
                <h5>Sales Tab Summary</h5>
                <div class="table-responsive">
                    <table id="customers_table" class="table report-table" style="border-collapse: separate;">
                        <thead>
                        <tr>
                            <th>Sales Tab</th>
                            <th>Pre-Tax</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sale_tab_summary as $re2)
                            <tr>
                                <?php
                                $asPreTax = sprintf('%0.2f', $re2->Pre_Tax);
                                $Total = sprintf('%0.2f', $re2->Total);
                                ?>
                                <td>{{$re2->Sale_Table}}</td>
                                <td>JD {{$asPreTax}}</td>
                                <td>JD {{$Total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-md-4">
                @if(count($payment_types_summary) > 0)
                    <h5>Payment Types Summary</h5>
                    <div class="table-responsive">
                        <table id="customers_table" class="table report-table" style="border-collapse: separate;">
                            <thead>
                            <tr>
                                <th>Quantity</th>
                                <th>Payment Type</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_amount = 0; ?>
                            @foreach($payment_types_summary as $re3)
                                <tr>
                                    <?php
                                    $amount = sprintf('%0.2f', $re3->Amount);
                                    $total_amount += $amount;
                                    ?>
                                    <td>{{$re3->Quantity}}</td>
                                    <td>JD {{$re3->Payment_Type}}</td>
                                    <td>JD {{round($amount,1)}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <?php
                                $amount = sprintf('%0.2f', $re3->Amount);
                                ?>
                                <td></td>
                                <td>Total Payments</td>
                                <td>JD {{round($total_amount,1)}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                @endif
            </div>
        </div>

        <hr>

        <div class="row">

            <div class="col-md-4">
                <h5>Tax Summary</h5>
                <div class="table-responsive">
                    <table id="customers_table" class="table report-table" style="border-collapse: separate;">
                        <thead>
                        <tr>
                            <th>Sale Tab</th>
                            <th>Tax Type</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_taxes = 0; ?>
                        @foreach($tax_summary as $re4)
                            <tr>
                                <?php
                                $amount = sprintf('%0.2f', $re4->Amount);
                                $total_taxes += $amount
                                ?>
                                <td>{{$re4->Tab_name}}</td>
                                <td>Sales Tax ({{$re4->Tax_Type}}%)</td>
                                <td>JD {{$amount}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td>Total Taxes</td>
                            <td>JD {{$total_taxes}}</td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-4">
                <h5>Voided Sales</h5>
                <div class="table-responsive">
                    <table id="VoidedSales" class="table table2 report-table" style="border-collapse: separate;">
                        <thead>
                        <tr>
                            <th>Sales ID</th>
                            <th>Payment</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total_voided = 0; ?>
                        @foreach($voided_sales as $re5)
                            <tr>
                                <?php
                                $amount6 = sprintf('%0.2f', $re5->Total);
                                $total_voided += $amount6;
                                ?>
                                <td>{{$re5->Sale_Id}}</td>
                                <td>{{$re5->Payment_Type}}</td>
                                <td>JD {{$amount6}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td>Total Voided</td>
                            <td>JD {{round($total_voided,1)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    @else
        <p>There are no sales during the selected period.</p>
    @endif
@endsection


@section('page_js')
    <script src="{{asset('assets/js/FileSaver.js')}}"></script>
    <script src="{{asset('assets/js/tableexport.min.js')}}"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChart2);
        google.charts.setOnLoadCallback(drawChart);

        function drawChart2() {

            var data2 = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                    @foreach($product_Types_Summary  as $data)
                    <?php $total_a = $data->Total;?>
                ['{{$data->Product_Types}}',     {{$total_a}}],
                @endforeach
            ]);

            var options2 = {
                chartArea: {left: 20, top: 0, width: '100%', height: '75%'}
            };
            var chart2 = new google.visualization.PieChart(document.getElementById('piechart'));
            chart2.draw(data2, options2);
        }

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                    @foreach($products_Summary  as $data)
                    <?php $total_a = $data->Total;?>
                ['{{$data->item}}',     {{$total_a}}],
                @endforeach
            ]);

            var options = {
                chartArea: {left: 20, top: 0, width: '100%', height: '75%'}
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
            chart.draw(data, options);
        }

    </script>



@endsection