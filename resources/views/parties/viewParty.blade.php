@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tempusdominus-bootstrap-4.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="row mt-3">
            <div class="col-sm-3 mb-4">
                <a href="{{route('parties')}}" class="btn bg-blue btn-add">Back</a>
            </div>
            <div class="col-sm-3 mb-4">
                <a href="{{route('editParty',$party->id)}}" class="btn bg-blue btn-add">Edit</a>
            </div>
            <div class="col-sm-3 mb-4">
                <a href="{{route('deleteParty',$party->id)}}" class="btn bg-red btn-add">Delete</a>
            </div>
        </div>
        <table id="customers_table" class="table table-hovered"  style="border-collapse: separate;">
            <thead class="thead-light">
            <tr>
                <th>Title</th>
                <th>Contact Name</th>
                <th width="150">Phone</th>
                <th>Date</th>
                <th>Form</th>
                <th>To</th>
                <th width="140">Number Of Kids</th>
                <th>Price</th>
                <th width="200">Details</th>
            </tr>
            </thead>
            <tbody>
            @php
                $from = new \DateTime($party->from_party);
                $to = new \DateTime($party->to_party);
            @endphp
            <tr>
                <td>{{$party->party_title}}</td>
                <td>{{$party->contact_name}}</td>
                <td>{{$party->phone}}</td>
                <td>{{$party->date_party}}</td>
                <td>{{$from->format('H:i')}}</td>
                <td>{{$to->format('H:i')}}</td>
                <td>{{$party->number_kids}}</td>
                <td>JD {{$party->price}}</td>
                <td>{!!$party->party_details!!}</td>
            </tr>
            </tbody>
        </table>
        {{--<div class="page-title mt-3">--}}
            {{--<h4 style="color: #29B6F6">Title: <span style="font-size: 18px;color: #000;">{{$party->party_title}}</span>--}}
            {{--</h4>--}}
        {{--</div>--}}
     {{----}}
        {{--<h4 style="color: #29B6F6">Details:</h4>--}}
        {{--{!! $party->party_details; !!}--}}

        {{--<h4 style="color: #29B6F6">Contact Name: <span--}}
                    {{--style="font-size: 18px;color: #000;">{{$party->contact_name}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">Phone: <span style="font-size: 18px;color: #000;">{{$party->phone}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">Date: <span style="font-size: 18px;color: #000;">{{$party->date_party}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">Form: <span--}}
                    {{--style="font-size: 18px;color: #000;">{{$from->format('H:i')}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">To: <span style="font-size: 18px;color: #000;">{{$to->format('H:i')}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">Number Of Kids: <span--}}
                    {{--style="font-size: 18px;color: #000;">{{$party->number_kids}}</span></h4>--}}
        {{--<h4 style="color: #29B6F6">Price: <span style="font-size: 18px;color: #000;">{{$party->price}}</span></h4>--}}

    </div>
@endsection

@section('page_js')
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <script type="text/javascript" src="{{asset('assets/js/empusdominus-bootstrap-4.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'H:m'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'H:m'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'YYYY/MM/DD'
            });
        });
    </script>
@endsection
