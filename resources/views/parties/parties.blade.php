@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">



@endsection

@section('content')
    <div class="customers">

        <div class="row  mb-4 mb-3">
            <div class="page-title col-sm-2">
                <h1>Booking</h1>
            </div>
            <div class="col-sm-3">
                <a href="{{route('viewAddParty')}}" class="btn bg-blue btn-add">Add Booking</a>
            </div>
        </div>
    </div>
    {!! $calendar_details->calendar() !!}
    <div class="tag hide" id="cal-info">

    </div>
@endsection

@section('page_js')
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/fullcalendar.min.js')}}"></script>
    {!! $calendar_details->script() !!}

@endsection
