@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tempusdominus-bootstrap-4.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Add Booking</h3>
        </div>
        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('parties')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>
        <form action="{{route('addParty')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers"
              onSubmit="return checkDate();">
            {{csrf_field()}}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="party_title" class="col-md-3 col-form-label">Booking Title:</label>
                        <div class="col-md-9">
                            <select name="party_title" id="party_title" class="form-control" style="height: 45px">
                                <option value="School Booking">School Booking</option>
                                <option value="Birthday Booking">Birthday Booking</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="party_details" class="col-md-12 col-form-label">Booking Details:</label>
                        <div class="col-md-12">
                            <textarea id="party_details" name="party_details"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="contact_name" class="col-sm-3 col-form-label">Contact Name:</label>
                        <div class="col-sm-9">
                            <input type="text" id="contact_name" name="contact_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-sm-3 col-form-label">Phone:</label>
                        <div class="col-sm-9">
                            <input type="text" id="phone" name="phone" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="date_party" class="col-sm-3 col-form-label">Date:</label>
                        <div class="col-sm-9">
                            <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                                <input type="text" id="date_party" autocomplete="off"
                                       class="form-control datetimepicker-input"
                                       data-target="#datetimepicker3" data-toggle="datetimepicker" name="date_party"
                                       date- data-target="#datetimepicker3"/>
                                <div class="input-group-append" data-target="#datetimepicker3"
                                     data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="from_party" class="col-sm-3 col-form-label">From:</label>
                        <div class="col-sm-9">
                            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                <input type="text" id="from_party" autocomplete="off"
                                       class="form-control datetimepicker-input"
                                       data-toggle="datetimepicker" name="from_party"
                                       data-target="#datetimepicker1"/>
                                <div class="input-group-append" data-target="#datetimepicker1"
                                     data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="to_party" class="col-sm-3 col-form-label">To:</label>
                        <div class="col-sm-9">
                            <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                <input type="text" id="to_party" autocomplete="off"
                                       class="form-control datetimepicker-input"
                                       data-toggle="datetimepicker" name="to_party"
                                       data-target="#datetimepicker2"/>
                                <div class="input-group-append" data-target="#datetimepicker2"
                                     data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="number_kids" class="col-sm-3 col-form-label" style="font-size: 15px">Number Of Kids:</label>
                        <div class="col-sm-9">
                            <input type="text" id="number_kids" name="number_kids" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-3 col-form-label">Price:</label>
                        <div class="col-sm-9">
                            <input type="text" id="price" name="price" class="form-control">
                        </div>
                    </div>
                    <div class="error-a" id="error">Please choose right time!</div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-add bg-blue">Save Booking</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('page_js')
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <script type="text/javascript" src="{{asset('assets/js/empusdominus-bootstrap-4.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'H:m'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'H:m'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'YYYY/MM/DD'
            });
        });

        function hideAllErrors() {
            document.getElementById("error").style.display = "none";
        }

        function checkDate() {
            start = document.getElementById("from_party").value;
            end = document.getElementById("to_party").value;
            if (start === end) {
                document.getElementById("error").style.display = "block";
                return false;
            }
            var time1 = start.split(':');
            var time2 = end.split(':');
            for (var i = 0; i < time1.length; i++) {
                if (time2[i] < time1[i]) {
                    hideAllErrors();
                    document.getElementById("error").style.display = "block";
                    return false;
                }
            }
            return true;
        }
    </script>
@endsection
