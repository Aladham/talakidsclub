<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script language="JavaScript">
        function newPage(aValue) {
            window.location.href = '{{route('sale')}}';
        }

        window.setTimeout(newPage, 2000, true);

    </script>
    <style type="text/css">
        @page {
            margin: 1px;
        }

        table tr td:first-child {
            width: 70%;
        }
    </style>
</head>
<body link="#000000" vlink="#000000" alink="#000000" onLoad="window.print()">

<table style="font-size:1.2em;" width="340px">
    <tr align="center">
        <td><a href="#" style="text-decoration:none; font-weight:bold;">Tala Kids Club</a></td>
    </tr>
    <tr align="center">
        <td>Al Swefieh - Al Baraka Mall, 2nd Floor</td>
    </tr>
    <tr align="center">
        <td>Amman,</td>
    </tr>
    <tr align="center">
        <td>+96265824649</td>
    </tr>
    <tr align="center">
        <td>info@talakidsclub.com</td>
    </tr>
    <tr align="center">
        <td>www.talakidsclub.com</td>
    </tr>
    <tr align="center">
        <td>&nbsp;</td>
    </tr>
    <tr align="center">
        <td><?php  echo now()->format('l, F j, Y');?> @ <?php echo now()->format('g:i a') ?></td>
    </tr>
    <tr align="center">
        <td>&nbsp;&nbsp;Host: {{ Auth::user()->username }}</td>
    </tr>
    @if(!empty($data_customer))
        <tr align="center">
            <td>Customer: {{$data_customer->first_name}} {{$data_customer->last_name}}</td>
        </tr>
        <tr align="center">
            <td>Phone: {{$data_customer->primary_phone}}</td>
        </tr>
        <tr align="center">

            <td>
                @if(!empty($childs))
                    Child:
                    @foreach($childs as $child)
                        <?php $child = implode(', ', $child); ?>
                        {{$child}}
                    @endforeach
                @endif
            </td>
        </tr>
        <tr align="center">
            <td>&nbsp;</td>
        </tr>
        <tr align="center">
            <td>
                Number Of Tickets: {{$print_visit / 10}} (Two Hours)
            </td>
        </tr>
    @endif
</table>
<table style="font-size:1em; margin-bottom:20px;" border="0" width="340px">
    &nbsp;&nbsp;




    {{--@if($data_point->free_hours > 0)--}}
        {{--<tr>--}}
            {{--<td align="right"><b>Free Hours:</b></td>--}}
            {{--<td align="left"><b> {{$data_point->free_hours}} Hour</b></td>--}}
        {{--</tr>--}}
    {{--@endif--}}
    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>

    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>

    <tr>
        <td colspan="2" align="center"><b>All Sales Final - No Refunds</b></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><b><a style="text-decoration:none" href="JavaScript:window.print();">Thank
                    You!</a></b></td>
    </tr>
</table>
</body>
</html>
