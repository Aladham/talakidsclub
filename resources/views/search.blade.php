@extends('layouts.app')
@section('page_css')
    <link href="{{asset('assets/css/epoch_styles.css')}}" rel="stylesheet"/>

@endsection
@section('content')


    <div class="page-title">
        <h3>Search</h3>
    </div>

    {{--<div class="btn-mange">--}}
        {{--<div class="row">--}}


            {{--<div class="col-sm-4 mb-3">--}}
                {{--<a href="{{route('productSearch')}}" class="btn bg-blue">Product Search</a>--}}
            {{--</div>--}}


        {{--</div>--}}
    {{--</div>--}}

    <hr>
    {{--<p>Enter the @if(Auth::user()->type != 2) {{"Sale ID,"}} @endif customer's last name or date to search for any--}}
        {{--matching sales in the system. (Run a blank search to see the last 50 sales.)--}}
    {{--</p>--}}

    <div class="row datepicker-search">
        <div class="col-md-4"></div>
        <div class="col-md-4 bg-silver p-20">
            <form action="{{route('searchSale')}}" method="get">
               <div class="row">
                   <div class="col-12">
                       <label for="SaleID">From</label>
                       <input autocomplete="off"  type="text" id="SaleID" name="from" class="form-control" value="{{now()->format('Y-m-d')}}">
                   </div>
                   <div class="col-12 mt-3">
                       <label for="SaleID2">To</label>
                       <input autocomplete="off"  type="text" id="SaleID2" name="to" class="form-control" value="{{now()->format('Y-m-d')}}">
                   </div>
                   <div class="col-12">
                       <div class="btn-mange mt-3">
                           <button type="submit" class="btn bg-blue">Run Search</button>
                       </div>
                   </div>
               </div>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
@endsection
@section('page_js')

    <script src="{{asset('assets/js/epoch_classes.js')}}"></script>
    <script>
        var cal, cal2;
        window.onload = function ()
        {
            cal  = new Epoch('epoch_popup','popup',document.getElementById('SaleID'));
            cal2  = new Epoch('epoch_popup','popup',document.getElementById('SaleID2'));
        }
     </script>
@endsection