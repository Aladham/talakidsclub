<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tala Kids Club</title>
    <link rel="icon" href="{{asset('assets/login/img/favicon.png')}}" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="{{asset('assets/login/css/icons/icomoon/styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    @yield('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">


    <script src="{{asset('assets/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="logo">
                    <img src="{{asset('assets/login/img/logo.png')}}" class="img-fluid" alt="logo">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row no-gutters">
                    <a href="{{route('saleEmpty')}}" class="col-sm-2">
                        <div class="tab-header bg-blue">
                            <i class=" icon-cart-add2"></i>
                            <span>Sales</span>
                        </div>
                    </a>
                    <a href="{{route('customers')}}" class="col-sm-2">
                        <div class="tab-header bg-green">
                            <i class=" icon-users4"></i>
                            <span>Customers</span>
                        </div>
                    </a>
                    <a href="{{route('search')}}" class="col-sm-2">
                        <div class="tab-header bg-bronze">
                            <i class="icon-search4"></i>
                            <span>Search</span>
                        </div>
                    </a>
                    <a href="{{route('parties')}}" class="col-sm-2">
                        <div class="tab-header bg-pink">
                            <i class=" icon-calendar"></i>
                            <span>Calendar</span>
                        </div>
                    </a>
                    @if(Auth::user()->type == 0)
                    {{--<a href="{{route('managePage')}}" class="col-sm-2">--}}
                        {{--<div class="tab-header bg-red">--}}
                            {{--<i class="icon-folder-open2"></i>--}}
                            {{--<span>Manage</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    <a href="{{route('adminPage')}}" class="col-sm-2">
                        <div class="tab-header bg-slate-blue">
                            <i class=" icon-user"></i>
                            <span>Admin</span>
                        </div>
                    </a>
                        @endif
                </div>

            </div>
            <div class="col-sm-2 text-right">
                <h5 class="username ">
                    {{ Auth::user()->username }}
                </h5>
                <a href="#" class="logout"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="icon-exit"></i> <span>Logout</span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</header>

<div class="container">
    @yield('content')
</div>
@yield('modal')
@yield('page_js')
</body>
</html>
