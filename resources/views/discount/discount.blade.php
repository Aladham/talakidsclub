@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Discount Amounts</h3>
            <span>(Enter -1 for 100%)</span>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        @if(session()->has('message_error'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message_error')}}</span>
            </div>
        @endif
        <div class="row mt-3">
            <div class="col-sm-3 mb-4">
                <a href="{{route('adminPage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('addDiscount')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="amount" class="col-sm-4 col-form-label">Amount:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Discount</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if(count($data)>0)
        @if(session()->has('messageDelete'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('messageDelete')}}</span>
            </div>
        @endif
        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Amount</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td>
                            @if($row->amount < 1)
                                <?php $number = round( $row->amount * 100 ) ?>
                                {{preg_replace('/-/','',$number)}} {{"%"}}
                            @else
                                {{$row->amount}} {{"JD"}}
                            @endif
                        </td>
                        <td class="text-center action-btn">
                            <a href="{{route('editDiscount',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deleteDiscount',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
        @endif
        <hr>

    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection