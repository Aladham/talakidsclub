@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Edit Discount Amount</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        @if(session()->has('message_error'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message_error')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('getDiscount')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updateDiscountReason',$data->id)}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="reason" class="col-sm-4 col-form-label">Reason:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="reason" name="reason" value="{{$data->reason}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Discount Amount</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>
@endsection
