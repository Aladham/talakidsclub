@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Employees</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddEmployee')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="username" class="col-sm-4 col-form-label">Username:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="username" name="username" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="employeeType" class="col-sm-4 col-form-label">Employee Type:</label>
                        <div class="col-sm-7">
                            <select id="employeeType" name="employeeType" class="form-control">
                                <option value="0">Administrator</option>
                                <option value="1">Host</option>
                                {{--<option value="2">Tax</option>--}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-4 col-form-label">Password:</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="firstName" class="col-sm-4 col-form-label">First Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="firstName" name="firstName" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastName" class="col-sm-4 col-form-label">Last Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="lastName" name="lastName" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-sm-4 col-form-label">Phone:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="phone" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Employee</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Employee Type</th>
                    <th scope="col">Employee Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td class="capitalize">{{$row->username}}</td>
                        <td>
                            @if($row->type == 0)
                                {{"Administrator"}}
                            @elseif($row->type == 1)
                                {{"Host"}}
                            @else
                                {{"Tax"}}
                            @endif
                        </td>
                        <td>{{$row->first_name}} {{$row->last_name}}</td>
                        <td>{{$row->phone}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('editEmployee',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deleteEmployee',$row->id)}}" onclick="confirmDelete()"
                               class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        $('.btn-package').on('click', function () {
            $('.add_package').fadeToggle(500);
        });

        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection