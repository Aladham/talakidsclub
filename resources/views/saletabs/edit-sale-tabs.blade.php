@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Edit Sale Tab</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('SaleTab')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updateSaleTab',$data->id)}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Tab Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="weight" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="weight" name="weight" value="{{$data->order}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Sale Tab</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        $('.btn-package').on('click', function () {
            $('.add_package').fadeToggle(500);
        });
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection