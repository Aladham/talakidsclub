@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Sale Tabs</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        @if(session()->has('error_message'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('error_message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddSaleTab')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Tab Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="weight" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="weight" name="weight" value="0">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Sale Tab</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if(count($data)>0)
        @if(session()->has('messageDelete'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span>{{session()->get('messageDelete')}}</span>
            </div>
        @endif
        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Deactivate</th>
                    <th scope="col">Name</th>
                    <th scope="col">Order</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td><a href="{{route('deactivateSaleTab',$row->id)}}" class="underline-a">Deactivate</a></td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->order}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('editSaleTab',$row->id)}}" class="btn bg-blue">Edit</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
        @endif
        <hr>


        @if(count($deactivate)>0)
        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Activate</th>
                    <th scope="col">Name</th>
                    <th scope="col">Order</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($deactivate as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td><a href="{{route('activateSaleTab',$row->id)}}" class="underline-a">Activate</a></td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->order}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('deleteSaleTab',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>

            @endif

    </div>
@endsection


@section('page_js')
    {{--<script src="{{asset('assets/js/datatables.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/js/main.js')}}"></script>--}}
    <script>
        $('.btn-package').on('click', function () {
            $('.add_package').fadeToggle(500);
        });
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection