@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Party Products</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddPartyProduct')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-4 col-form-label">Description:</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" id="description" name="description" style="min-height: 200px;"> </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ProductType" class="col-sm-4 col-form-label">Product Type:</label>
                        <div class="col-sm-7">
                            <select id="ProductType" name="ProductType" class="form-control">
                                @foreach(\App\Product::getProductTypeParty() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-4 col-form-label">Price:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price" name="price">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deposit" class="col-sm-4 col-form-label">Deposit:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="deposit" name="deposit">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tax" class="col-sm-4 col-form-label">Tax:</label>
                        <div class="col-sm-7">
                            <select id="tax" name="tax" class="form-control">
                                @foreach(\App\Product::getTax() as $row)
                                    <option value="{{$row->amount}}">{{$row->amount}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="size" class="col-sm-4 col-form-label">Size:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="size" name="size">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price_per_extra_child" class="col-sm-4 col-form-label">Price Per Extra Child:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price_per_extra_child" name="price_per_extra_child">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="duration" class="col-sm-4 col-form-label">Duration:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="duration" name="duration">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="order" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="order" name="order" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Product Party</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Price</th>
                    <th scope="col">Deposit</th>
                    <th scope="col">Tax</th>
                    <th scope="col" style="width: 100px;">Price Per Extra Child</th>
                    <th scope="col">Size</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Order</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->getProductTypeForProductParty->name}}</td>
                        <td>{{$row->description}}</td>
                        <td >{{$row->price}} JD</td>
                        <td >{{$row->deposit}}</td>
                        <td>{{$row->tax}}</td>
                        <td>{{$row->price_per_extra_child}}</td>
                        <td>{{$row->size}}</td>
                        <td>{{$row->duration}}</td>
                        <td>{{$row->order_p}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('editPartyProduct',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deletePartyProduct',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection