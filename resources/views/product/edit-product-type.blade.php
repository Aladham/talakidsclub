@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Edit Product Type</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('productType')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updateProductType',$data->id)}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Product Type Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="SaleTab" class="col-sm-4 col-form-label">Sale Tab:</label>
                        <div class="col-sm-7">
                            <select id="SaleTab" name="SaleTab" class="form-control">
                                <option value="{{$data->fk_sale_tab_id}}">{{$data->getSaleTypeForProduct->name}}</option>
                                @foreach(\App\ProductType::getSaleTab() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="buttonColor" class="col-sm-4 col-form-label">Button Color:</label>
                        <div class="col-sm-7">
                            <select id="buttonColor" name="buttonColor" class="form-control">
                                <option value="{{$data->button_colour}}" selected>{{$data->button_colour}}</option>
                                <option value="black">Black</option>
                                <option value="blue">Blue</option>
                                <option value="green">Green</option>
                                <option value="purple">Purple</option>
                                <option value="orange">Orange</option>
                                <option value="red">Red</option>
                                <option value="yellow">Yellow</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="weight" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="weight" name="weight" value="{{$data->weight}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Product Type</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

@endsection