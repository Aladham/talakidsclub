@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title mt-4">
            <h3>Product Search</h3>
        </div>


        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Price</th>
                    <th scope="col">Tax</th>
                    <th scope="col">Points</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr data-index="{{$row->id}}" data-position="{{$row->position}}">
                        <td scope="row">{{$i}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->getProductTypeForProduct->name}}</td>
                        <td class="capitalize">{{$row->price}} JD</td>
                        <td>{{$row->tax}}</td>
                        <td>{{$row->point}}</td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>

    <script src="{{asset('assets/js/main.js')}}"></script>

@endsection