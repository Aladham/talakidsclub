@extends('layouts.app')
@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Product Types</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        @if(session()->has('error_message'))
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('error_message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddProductType')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Product Type Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="SaleTab" class="col-sm-4 col-form-label">Sale Tab:</label>
                        <div class="col-sm-7">
                            <select id="SaleTab" name="SaleTab" class="form-control">
                                @foreach(\App\ProductType::getSaleTab() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="parties" class="col-sm-4 col-form-label">Parties:</label>
                        <div class="col-sm-7">
                            <input type="checkbox" value="1" id="parties" name="parties">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="buttonColor" class="col-sm-4 col-form-label">Button Color:</label>
                        <div class="col-sm-7">
                            <select id="buttonColor" name="buttonColor" class="form-control">
                                <option value="black">Black</option>
                                <option value="blue">Blue</option>
                                <option value="green">Green</option>
                                <option value="purple">Purple</option>
                                <option value="orange">Orange</option>
                                <option value="red">Red</option>
                                <option value="yellow">Yellow</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="weight" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="weight" name="weight" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Product Type</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="table-responsive">
            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Sale Tab</th>
                    <th scope="col">Button Color</th>
                    <th scope="col">Order</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr>
                        <td scope="row">{{$i}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->getSaleTypeForProduct->name}}</td>
                        <td class="capitalize">{{$row->button_colour}}</td>
                        <td>{{$row->weight}}</td>
                        <td class="text-center action-btn">
                            <a href="{{route('editProductType',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deleteProductType',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection