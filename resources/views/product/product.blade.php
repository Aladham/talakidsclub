@extends('layouts.app')
@section('page_css')
    {{--<link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">--}}
@endsection
@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Manage Products</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('managePage')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('AddProduct')}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Product Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ProductType" class="col-sm-4 col-form-label">Product Type:</label>
                        <div class="col-sm-7">
                            <select id="ProductType" name="ProductType" class="form-control">
                                @foreach(\App\Product::getProductType() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-4 col-form-label">Price:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price" name="price">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tax" class="col-sm-4 col-form-label">Tax:</label>
                        <div class="col-sm-7">
                            <select id="tax" name="tax" class="form-control">
                                @foreach(\App\Product::getTax() as $row)
                                    <option value="{{$row->amount}}">{{$row->amount}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="point" class="col-sm-4 col-form-label">Points:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="point" name="point" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Add Product</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="table-responsive">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <table id="customers_table" class="table table-hovered">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Product Type</th>
                    <th scope="col">Price</th>
                    <th scope="col">Tax</th>
                    <th scope="col">Points</th>
                    <th scope="col">Order</th>
                    <th scope="col">Online Party</th>
                    <th class="text-center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $i = 1; ?>
                @foreach($data as $row)
                    <tr data-index="{{$row->id}}" data-position="{{$row->position}}">
                        <td scope="row">{{$i}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->getProductTypeForProduct->name}}</td>
                        <td class="capitalize">{{$row->price}} JD</td>
                        <td>{{$row->tax}}</td>
                        <td>{{$row->point}}</td>
                        <td>{{$row->order_p}}</td>
                        <th scope="col">{{$row->online_party}}</th>
                        <td class="text-center action-btn">
                            <a href="{{route('editProduct',$row->id)}}" class="btn bg-blue">Edit</a>
                            <a href="{{route('deleteProduct',$row->id)}}" onclick="confirmDelete()" class="btn bg-red">Delete</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('page_js')
    {{--<script src="{{asset('assets/js/datatables.min.js')}}"></script>--}}
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>

    <script src="{{asset('assets/js/main.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('table tbody').sortable({
                update: function (event, ui) {
                    $(this).children().each(function (index) {
                        if ($(this).attr('data-position') != (index + 1)) {
                            $(this).attr('data-position', (index + 1)).addClass('updated');
                        }
                    });
                    saveNewPositions();
                }
            });
        });

        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'),$(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
               url:'{{route('position')}}',
                method:'POST',
                dataType:'text',
                data:{
                   update:1,
                    positions:positions
                },
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                success: function (response) {
                    console.log(response);
                }
            });
        }


        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }
    </script>.

@endsection