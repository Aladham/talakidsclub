@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Edit Product Party</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('getPartyProduct')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updatePartyProduct',$data->id)}}" method="post" class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-4 col-form-label">Description:</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" id="description" name="description" style="min-height: 200px;"> {{$data->description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ProductType" class="col-sm-4 col-form-label">Product Type:</label>
                        <div class="col-sm-7">
                            <select id="ProductType" name="ProductType" class="form-control">
                                <option value="{{$data->fk_product_type_id}}">{{$data->getProductTypeForProductParty->name}}</option>
                                @foreach(\App\Product::getProductTypeParty() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-4 col-form-label">Price:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price" name="price" value="{{$data->price}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deposit" class="col-sm-4 col-form-label">Deposit:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="deposit" name="deposit" value="{{$data->deposit}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tax" class="col-sm-4 col-form-label">Tax:</label>
                        <div class="col-sm-7">
                            <select id="tax" name="tax" class="form-control">
                                <option value="{{$data->tax}}">{{$data->tax}}</option>
                                @foreach(\App\Product::getTax() as $row)
                                    <option value="{{$row->amount}}">{{$row->amount}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="size" class="col-sm-4 col-form-label">Size:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="size" name="size" value="{{$data->size}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price_per_extra_child" class="col-sm-4 col-form-label">Price Per Extra Child:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price_per_extra_child" name="price_per_extra_child" value="{{$data->price_per_extra_child}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="duration" class="col-sm-4 col-form-label">Duration:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="duration" name="duration" value="{{$data->duration}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="order" class="col-sm-4 col-form-label">Order:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="order" name="order" value="{{$data->order_p}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Product Party</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

@endsection