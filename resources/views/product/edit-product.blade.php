@extends('layouts.app')

@section('content')

    <div class="sale-tab">
        <div class="page-title">
            <h3>Edit Product</h3>
        </div>

        @include('layouts.errors')
        @if(session()->has('message'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                            class="sr-only">Close</span></button>
                <span class="text-semibold">{{session()->get('message')}}</span>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('getProduct')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="{{route('updateProduct',$data->id)}}" method="post"
                      class="bg-silver p-20 mb-5 form-add-customers">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Product Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ProductType" class="col-sm-4 col-form-label">Product Type:</label>
                        <div class="col-sm-7">
                            <select id="ProductType" name="ProductType" class="form-control">
                                <option value="{{$data->fk_product_type_id}}">{{$data->getProductTypeForProduct->name}}</option>
                                @foreach(\App\Product::getProductType() as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-4 col-form-label">Price:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="price" name="price" value="{{$data->price}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tax" class="col-sm-4 col-form-label">Tax:</label>
                        <div class="col-sm-7">
                            <select id="tax" name="tax" class="form-control">
                                @foreach(\App\Product::getTax() as $row)
                                    <option value="{{$row->amount}}" @if($row->amount == $data->tax){{
                                    "selected"
                                    }} @endif>{{$row->amount}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="point" class="col-sm-4 col-form-label">Points:</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="point" name="point" value="{{$data->point}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="notes" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-add bg-blue">Update Product</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
@endsection


@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

@endsection