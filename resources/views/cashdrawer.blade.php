<html>
<head>
    <script language="JavaScript">
        function newPage(aValue) {
            window.location.href='{{URL::previous()}}';
        }
        window.setTimeout(newPage, 2000, true); //note: this will not work in IE
        //setTimeout("top.location.href='SaleNew.php'",1000);
    </script>
    <style type="text/css">
        @media print { .no-print, .no-print * { display: none !important; } };
    </style>
</head>
<body onLoad="window.print()">
<a style="text-decoration:none;" class="no-print" href="{{route('sale')}}">Back</a>
</body>
</html>