<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script language="JavaScript">
        function newPage(aValue) {
            window.location.href = '{{route('sale')}}';
        }

        window.setTimeout(newPage, 2000, true);

    </script>
    <style type="text/css">
        @page {
            margin: 1px;
        }


    </style>
</head>
<body link="#000000" vlink="#000000" alink="#000000" onLoad="window.print()">

<table style="font-size:1.2em;" width="340px">
    <tr align="center">
        <td><a href="#" style="text-decoration:none; font-weight:bold;">Tala Kids Club</a></td>
    </tr>
    <tr align="center">
        <td>Al Swefieh - Al Baraka Mall, 2nd Floor</td>
    </tr>
    <tr align="center">
        <td>Amman,</td>
    </tr>
    <tr align="center">
        <td>+96265824649</td>
    </tr>
    <tr align="center">
        <td>info@talakidsclub.com</td>
    </tr>
    <tr align="center">
        <td>www.talakidsclub.com</td>
    </tr>
    <tr align="center">
        <td>&nbsp;</td>
    </tr>
    <tr align="center">
        <td>    {{ $data_sale->created_at->format('l, F j, Y')}} @ {{$data_sale->created_at->format('g:i a')}}</td>
    </tr>
    <tr align="center">
        <td></td>
    </tr>
    @if(!empty($data_customer))
        <tr align="center">
            <td>Customer: {{$data_customer->first_name}} {{$data_customer->last_name}}</td>
        </tr>
        <tr align="center">
            <td>Phone: {{$data_customer->primary_phone}}</td>
        </tr>
        <tr align="center">
            <td>
                @if($data_sale->child != "null" && !empty($data_sale->child))
                    Child:
                    <?php
                    $data = json_decode($data_sale->child);
                    echo implode(', ', $data);
                    ?>
                @endif
            </td>
        </tr>
    @endif
</table>
<table style="font-size:1em; margin-bottom:20px;" border="0" width="340px">
    <tbody>
    <tr>
        <td><b>Sale Items:</b></td>
    </tr>


    @foreach($data_sale->getProductForSale as $items_sale)
        <tr>
            <td width="90px" align="left" class="no-s">
                <?php $price_items = $items_sale->price * $items_sale->quantity; ?>
                {{$items_sale->quantity}} {{$items_sale->item}}

            </td>
            <td align="right">
                <?php $discount_a = $items_sale->discount;

                $number = sprintf('%0.2f', $price_items);
                ?>
                JD {{$number}}
            </td>
        </tr>&nbsp;&nbsp;
        <tr>
            <td width="90px" align="left" class="no-s">
            </td>
            <td align="right">
                @if($discount_a < 1 && $discount_a != 0 && $discount_a > -1)
                    ({{abs($discount_a) * 100}} % off)
                @elseif($discount_a == -1)
                    ({{abs($discount_a) * 100}} % off)
                @elseif($discount_a == 0)
                    {{' '}}
                @else
                      (JD {{$discount_a}} off )
                @endif


            </td>
        </tr>&nbsp;&nbsp;
    @endforeach
    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>
    <tr>
        <td align="right"><b>Subtotal:</b></td>
        <td align="right"><b>JD {{$data_sale->sub_total}}</b></td>
    </tr>
    <tr>
        <td align="right"><b>Sales Tax:</b></td>
        <td align="right"><b>JD {{$data_sale->tax}}</b></td>
    </tr>
    <tr>
        <td align="right"><b>Total:</b></td>
        <td align="right"><b>JD {{sprintf('%0.2f',$data_sale->total)}}</b></td>
    </tr>
    <tr>
        <td align="left"><b>Payments:</b></td>
    </tr>
    <tr>
        <td align="right"><b>{{$data_sale->payment}} ({{$data_sale->created_at->format('F j, Y')}}):</b></td>
        <td align="right">JD {{sprintf('%0.2f',$data_sale->total)}}</td>
    </tr>
    @if($data_sale->payment == "Cash")
        <tr>
            <td align="right"><b>Cash Received:</b></td>
            <?php $number_cash = sprintf('%0.2f', $data_sale->cash_received); ?>
            <td align="right">JD {{$number_cash}}</td>
        </tr>
        <tr>
            <td align="right"><b>Change:</b></td>
            <?php $number_change_p = sprintf('%0.2f', $data_sale->change_p); ?>
            <td align="right">JD {{$number_change_p}}</td>
        </tr>
    @endif
    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>
    <tr>
        <td align="left"><b>Total Payments:</b></td>
        <td align="right"><b>JD {{sprintf('%0.2f',$data_sale->total)}}</b></td>
    </tr>

    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>
    @if(!empty($data_sale->note))
        <tr>
            <td colspan="2" align="left"><b>Notes:</b></td>
        </tr>
        <tr>
            <td colspan="2" align="left">{{$data_sale->note}}</td>
        </tr>
    @endif
    <tr>
        <td align="right"><b>&nbsp;</b></td>
        <td><b>&nbsp;</b></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><b>All Sales Final - No Refunds</b></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><b><a style="text-decoration:none" href="JavaScript:window.print();">Thank
                    You!</a></b></td>
    </tr>
    </tbody>
</table>
</body>
</html>