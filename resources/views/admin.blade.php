@extends('layouts.app')

@section('content')


    <div class="page-title">
        <h3>Administrative Settings</h3>
    </div>

    <div class="btn-mange">
        <div class="row">
            <div class="col-sm-4 mb-3">
                <a href="{{route('getTax')}}" class="btn bg-blue">Tax Rates</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('getPayment')}}" class="btn bg-blue">Payment Type</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('getDiscount')}}" class="btn bg-blue">Discount</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('getDiscountReason')}}" class="btn bg-blue">Discount Reasons</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('SaleTab')}}" class="btn bg-blue">Sale Tabs</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('viewEmployees')}}" class="btn bg-blue">Employees</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('getProduct')}}" class="btn bg-blue">Products</a>
            </div>
            {{--<div class="col-sm-4 mb-3">--}}
                {{--<a href="{{route('getPartyProduct')}}" class="btn bg-blue">Products: Parties</a>--}}
            {{--</div>--}}

            <div class="col-sm-4 mb-3">
                <a href="{{route('productType')}}" class="btn bg-blue">Product Type</a>
            </div>
            {{--<div class="col-sm-4 mb-3">--}}
            {{--<a href="{{route('getDeletedCustomers')}}" class="btn bg-blue">Deleted Customers</a>--}}
            {{--</div>--}}
            <div class="col-sm-4 mb-3">
                <a href="{{route('getAllSale')}}" class="btn bg-blue">View Sales</a>
            </div>
            <div class="col-sm-4 mb-3">
                <a href="{{route('reports')}}" class="btn bg-blue">Reports</a>
            </div>
        </div>
    </div>

@endsection