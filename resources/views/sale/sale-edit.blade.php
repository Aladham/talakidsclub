@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Sale </h3>
        </div>
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('DataSale',$data->id)}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row">
            <div class="col-3">

                <form action="{{route('updateSale',$data->id)}}" method="post">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <select name="payment" id="" class="form-control">
                        <option value="Cash" @if($data->payment == "Cash") {{"selected"}} @endif>Cash</option>
                        <option value="Visa" @if($data->payment == "Visa") {{"selected"}} @endif>Visa</option>
                    </select>
                    <button type="submit" class="btn bg-blue btn-add mt-4">Update</button>
                </form>
            </div>
        </div>



    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
@endsection