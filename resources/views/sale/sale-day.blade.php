@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
    <link href="{{asset('assets/css/epoch_styles.css')}}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>List of Sales </h3>
        </div>
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('getAllSale')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <form action="{{route('saleDay')}}" method="get">
            <div class="row">
                <div class="col-md-3">
                    <input data-date-format="yyyy-mm-dd" autocomplete="off" type="text" id="SaleID" name="data"
                           class="form-control search-sale" value="{{$data_req}}" >
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn bg-blue btn-add search-sale">Search</button>
                </div>
            </div>
        </form>

        <form action="{{route('hideMultiSale')}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            @if(count($data) > 0)
                <div class="row mt-4">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <h5>Total Cash: JD {{$totalCash}}</h5>
                    </div>
                    <div class="col-md-3">
                        <h5> Total Visa: JD {{$totalVisa}}</h5>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2">
                        <input type="text" id="number" class="form-control search-sale mb-3" onkeyup="myFunction()">
                        <button type="submit" class="btn bg-blue btn-add search-sale">Hide</button>
                    </div>
                    <div class="col-md-3">
                        <h5>Total Selected Sales: JD <span class="total_select">0</span></h5>
                    </div>
                    <div class="col-md-3">
                        <h5>Total Sales:
                            <?php $total = 0; ?>
                            @foreach($data as $total_sale)
                                <?php $total += $total_sale->total?>
                            @endforeach
                            JD {{$total}}
                        </h5>

                    </div>
                    <div class="col-md-3">
                        <h5>Number Of Sales: {{count($data)}}</h5>
                    </div>
                </div>



                <h5 class="mt-4">Active</h5>
                <table id="customers_table" class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>

                        <th scope="col">Sale ID</th>
                        {{--<th scope="col">Tax ID</th>--}}
                        <th scope="col">Customer</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Total</th>
                        <th scope="col">Payment Date</th>
                        <th scope="col">Payment Type</th>
                        <th scope="col">Employee</th>
                        <th class="text-center" scope="col">Action</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php $i = 1; ?>
                    @if(count($data) > 0)
                    @foreach( $data as $row)
                        <tr>
                            <td class="text-center">{{$i}}</td>
                            {{--<td class="text-center underline-a">--}}
                                {{--<a href="{{route('DataSale',$row->id)}}">--}}
                                    {{--{{$row->id}}--}}
                                {{--</a>--}}
                            {{--</td>--}}
                            {{--<td class="text-center"><a--}}
                                        {{--href="{{route('DataSale',$row->id)}}">{{$row->order_s}}</a>--}}
                            {{--</td>--}}
                            <td class="text-center underline-a">
                                <a href="{{route('DataSale',$row->id)}}">
                                    {{$row->order_s}}
                                </a></td>

                            <td class="text-center"> @if($row->fk_customer != NULL || $row->fk_customer !='') {{$row->getCustomerForSale->first_name}} {{$row->getCustomerForSale->last_name}} @else{{"-"}} @endif</td>
                            <td class="text-center">@if($row->fk_customer != NULL || $row->fk_customer !='') {{$row->getCustomerForSale->primary_phone}} @else {{"-"}} @endif</td>
                            <td>{{$row->total}} JD</td>
                            <td>{{$row->created_at->format((' F j, Y'))}}</td>
                            <td>{{$row->payment}}</td>
                            <td>{{$row->getEmpForSale->username}}</td>
                            <td class="text-center action-btn">
                                <a href="{{route('hideSale',$row->id)}}" onclick="confirmDelete()"
                                   class="btn bg-red">Hide</a>
                            </td>
                            <td>
                                <input value="{{$row->id}}" class="checkbox" type="checkbox"
                                       name="checkSale[]"/>
                                <input type="hidden" value="{{$row->total}}" class="total_val"/>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
@endif
                    </tbody>
                </table>
            @endif
        </form>
        @if(count($data_deactive) > 0)

            <h5 class="mt-3">Inactive</h5>
            <form action="{{route('deleteSaleDay')}}">
                <input type="hidden" name="date" value="{{$data_req}}">
                <button type="submit" onclick="confirmDelete()"
                        class="btn bg-red ml-1" style="color: #FFF">Delete All</button>
            </form>

            <table id="customers_table" class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Sale ID</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Total</th>
                    <th scope="col">Payment Date</th>
                    <th scope="col">Payment Type</th>
                    <th scope="col">Employee</th>
                    <th scope="col">Activate</th>
                </tr>
                </thead>
                <tbody>


                <?php $i = 1; ?>
                @if(count($data_deactive) > 0)
                @foreach( $data_deactive as $row_active)
                    <tr>
                        <td class="text-center">{{$i}}
                        </td>
                        {{--<td class="text-center underline-a">--}}
                            {{--<a href="{{route('DataSale',$row_active->id)}}">--}}
                                {{--{{$row_active->id}}--}}
                            {{--</a>--}}
                        {{--</td>--}}
                        <td class="text-center underline-a">
                            <a href="{{route('DataSale',$row_active->id)}}">
                                {{$row_active->order_s}}
                            </a>
                        </td>

                        <td class="text-center"> @if($row_active->fk_customer != NULL || $row_active->fk_customer !='')  @if(!empty($row_active->getCustomerForSale->first_name) || !empty($row_active->getCustomerForSale->last_name)) {{$row_active->getCustomerForSale->first_name}} {{$row_active->getCustomerForSale->last_name}} @else{{"-"}} @endif @else{{"-"}} @endif</td>
                        <td class="text-center">@if($row_active->fk_customer != NULL || $row_active->fk_customer !='')  @if(!empty($row_active->getCustomerForSale->primary_phone)) {{$row_active->getCustomerForSale->primary_phone}} @else {{"-"}} @endif @else {{"-"}} @endif</td>
                        <td>{{$row_active->total}} JD</td>
                        <td>{{$row_active->created_at->format(' F j, Y')}}</td>
                        <td>{{$row_active->payment}}</td>
                        <td>{{$row_active->getEmpForSale->username}}</td>
                        <td> <a href="{{route('activeSale',$row_active->id)}}" class="btn bg-blue" style="color: #fff">Activate</a></td>

                    </tr>
                    <?php $i++; ?>
                @endforeach
                @endif
                </tbody>
            </table>
        @endif
    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    @if(Auth::user()->type == 0)
    <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    @endif
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{asset('assets/js/epoch_classes.js')}}"></script>

    <script>


        function confirmDelete() {

            if (!confirm("Are you sure?"))
                event.preventDefault();
        }

        function selectFunction() {
            var x = document.getElementById("mySelect").value;
            document.getElementById("demo").innerHTML = "You selected: " + x;
        }

        function myFunction() {
            var select_all = document.getElementById("select_all"); //select all checkbox
            var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items
            var total_val = document.getElementsByClassName("total_val"); //checkbox items
            var number_val = document.getElementById("number").value; //checkbox items


            var sum = 0

            // step 1 disablel all
            $('#customers_table').find('input[name="check[]"]').prop("checked", false);

            //step 2 enabled



            for (var i = 0; i < number_val * 2; i++) {
                if (i % 2 == 0) {
                    checkboxes[i].checked = true;
                    var total = parseInt(total_val[i].value);
                    sum += total;

                }
            }
            $('.total_select').text(sum);

        }

        $(document).ready(function () {
            $('.checkbox').change(function () {
                getValueXheckbox();
            });
        });

        function getValueXheckbox() {
            var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items
            var inputElems = document.getElementsByClassName("total_val"),
                count = 0,
                total_sales = 0;
            for (var i = 0; i < inputElems.length; i++) {
                if (checkboxes[i].checked) {
                    count +=parseInt(inputElems[i].value);

                }
            }
            $('.total_select').text(count);

        }
    </script>

    <script>
        var cal;
        window.onload = function () {
            cal = new Epoch('epoch_popup', 'popup', document.getElementById('SaleID'));
        };
    </script>

@endsection