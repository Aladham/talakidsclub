@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
    <link href="{{asset('assets/css/epoch_styles.css')}}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>List of Sales </h3>
        </div>
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('search')}}" class="btn bg-blue btn-add">Back</a>
            </div>
        </div>

        <div class="row datepicker-search">
            <div class="col-4"></div>
            <div class="col-md-4 bg-silver p-20">
                <form action="{{route('searchSale')}}" method="get">
                    <div class="row">
                        <div class="col-12">
                            <label for="SaleID">From</label>
                            <input autocomplete="off"  type="text" id="SaleID" name="from" class="form-control" value="{{$from}}">
                        </div>
                        <div class="col-12 mt-3">
                            <label for="SaleID2">To</label>
                            <input autocomplete="off"  type="text" id="SaleID2" name="to" class="form-control" value="{{$to}}">
                        </div>
                        <div class="col-12">
                            <div class="btn-mange mt-3">
                                <button type="submit" class="btn bg-blue">Run Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-4"></div>
        </div>

        <h5>Sales</h5>
        @if(count($data) > 0)
            <div class="table-responsive">
                <table id="customers_table" class="table2" style="width: 100%;    border-collapse: separate;">
                    <thead class="thead-light">
                    <tr>
                        {{--<th scope="col">#</th>--}}
                        <th scope="col" width="70">Sales ID</th>
                        <th scope="col">Receipts</th>
                        <th scope="col" width="120">Customer</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Product</th>
                        <th scope="col" width="50">Total</th>
                        <th scope="col" width="120">Payment Date</th>
                        <th scope="col" width="110">Payment Type</th>
                        <th scope="col">Employee</th>
                        {{--<th class="text-center" scope="col">Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>


                    <?php $i = 1; ?>
                    @foreach( $data as $row)
                        <tr>
                            {{--<td class="text-center underline-a"><a href="{{route('DataSale',$row->id)}}">{{$i}}</a></td>--}}
                            <td class="text-center">{{$row->order_s}}
                            </td>
                            <td class="text-center underline-a"><a href="{{route('receipts',$row->id)}}">Print</a></td>
                            <td class="text-center @if($row->fk_customer != NULL || $row->fk_customer !='') underline-a @endif"> @if($row->fk_customer != NULL || $row->fk_customer !='')
                                    <a href="{{route('viewCustomer',$row->getCustomerForSale->id)}}">{{$row->getCustomerForSale->first_name}} {{$row->getCustomerForSale->last_name}}</a>
                                @else{{"-"}} @endif</td>
                            <td class="text-center">@if($row->fk_customer != NULL || $row->fk_customer !='') {{$row->getCustomerForSale->primary_phone}} @else {{"-"}} @endif</td>
                            <td width="250">
                                @foreach($row->getProductForSale as $product)
                                    {{$product->item.' - '}}
                                @endforeach
                            </td>
                            <td>{{$row->total}} JD</td>
                            {{--@ {{$row->created_at->format((' g:i:s a'))}}--}}
                            <td>{{$row->created_at->format((' F j, Y'))}} </td>
                            <td>{{$row->payment}}</td>
                            <td>{{$row->getEmpForSale->username}}</td>
                            {{--<td class="text-center action-btn">--}}
                            {{--<a href="{{route('hideSale',$row->id)}}" onclick="confirmDelete()"--}}
                            {{--class="btn bg-red">Hide</a>--}}
                            {{--</td>--}}
                        </tr>
                        {{--<tr class="mb-3">--}}
                        {{--<td>Product:</td>--}}
                        {{--<td colspan="8">--}}
                        {{--@foreach($row->getProductForSale as $product)--}}
                        {{--{{$product->item.' - '}}--}}
                        {{--@endforeach--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        <?php $i++; ?>
                    @endforeach

                    </tbody>
                </table>
            </div>


        @else
            {{"Sorry there is no result!"}}
        @endif

    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{asset('assets/js/epoch_classes.js')}}"></script>
    <script>

        $('.table2').DataTable({
            "pageLength": 100,
            bSort: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [ 0, 2, 3, 5, 6, 7, 8 ]
                    }}
            ]
        });
        var cal, cal2;
        window.onload = function () {
            cal = new Epoch('epoch_popup', 'popup', document.getElementById('SaleID'));
            cal2 = new Epoch('epoch_popup', 'popup', document.getElementById('SaleID2'));
        };


    </script>

@endsection