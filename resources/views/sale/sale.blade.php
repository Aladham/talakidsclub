@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{asset('assets/css/datatables.min.css')}}">
@endsection

@section('content')

    <div class="customers">
        <div class="page-title">
            <h3>Sale </h3>
        </div>
        <div class="row">
            <div class="col-sm-3 mb-4">
                <a href="{{route('getAllSale')}}" class="btn bg-blue btn-add">Back</a>
            </div>
            <div class="col-sm-3 mb-4">
                <a href="{{route('editSale',$data->id)}}" class="btn bg-blue btn-add">Edit</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @if($data->fk_customer != NULL || $data->fk_customer !='')    <p><strong class="w-120 d-inline-block">Customer
                        Name:</strong> {{$data->getCustomerForSale->first_name}} {{$data->getCustomerForSale->last_name}}
                </p>

                <p><strong class="w-120 d-inline-block">Phone:</strong> {{$data->getCustomerForSale->primary_phone}}
                </p>
                @endif
                <p><strong class="w-120 d-inline-block">Total:</strong> {{$data->total}} JD</p>
                <p><strong class="w-120 d-inline-block">Employee:</strong> {{$data->getEmpForSale->username}} </p>
                <p><strong class="w-120 d-inline-block">Payment Type:</strong> {{$data->payment}} </p>
            </div>
        </div>


        @if(count($data_product) > 0)
            <h3>
                Product Of Sale
            </h3>
           <div class="table-responsive">
               <table id="customers_table" class="table">
                   <thead class="thead-light">
                   <tr>
                       <th class="text-center" scope="col">#</th>
                       <th scope="col">Name</th>
                       <th scope="col">Quantity</th>
                       <th scope="col">Price</th>
                       <th scope="col">Tax</th>
                       <th scope="col">Discount</th>
                   </tr>
                   </thead>
                   <tbody>


                   <?php $i = 1; ?>

                   @foreach($data_product as $row)
                       <tr>
                           <td class="text-center">{{$i}}</td>
                           <td>{{$row->item}}</td>
                           <td>{{$row->quantity}}</td>
                           <td>{{$row->price}}</td>
                           <td>{{$row->tax}}</td>
                           <td>{{$row->discount}}</td>
                       </tr>

                       <?php $i++; ?>

                   @endforeach
                   </tbody>
               </table>
           </div>
        @endif

    </div>

@endsection

@section('page_js')
    <script src="{{asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
@endsection