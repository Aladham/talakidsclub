<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPoints extends Model
{
    protected $table = "customers_point";
    protected $guarded = [];
}
