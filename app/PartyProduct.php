<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyProduct extends Model
{
    protected $guarded = [];

    public function getProductTypeForProductParty(){
        return $this->belongsTo('App\ProductType','fk_product_type_id');
    }
}
