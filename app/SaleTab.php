<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleTab extends Model
{
    protected $guarded = [];

    public function getProductTypeForSaleTab()
    {
        return $this->hasMany('App\ProductType', 'fk_sale_tab_id')
            ->orderBy('weight','ASC');
    }
}
