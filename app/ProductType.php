<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded=[];

    public static function getSaleTab(){
        return SaleTab::all()->where('active','=','0');
    }

    public function getSaleTypeForProduct(){
        return $this->belongsTo('App\SaleTab','fk_sale_tab_id');
    }

    public function getProductForProductType()
    {
        return $this->hasMany('App\Product', 'fk_product_type_id')
            ->orderBy('position');
    }

    public function getPartyProductForProductType()
    {
        return $this->hasMany('App\PartyProduct', 'fk_product_type_id')
            ->orderBy('order_p','ASC');
    }

}
