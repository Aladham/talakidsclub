<?php
namespace App\classes;


class Hellpers
{
    public static function nearly_round($value, $limit = 0.1) {
        $rounded = round($value);
        return abs($rounded - $value) < $limit ? $rounded : $value;
    }
}