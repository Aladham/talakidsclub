<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $guarded = [];

   public static function getDiscount(){
        $data = Discount::orderBy('amount')->get();
        return $data;
    }
}
