<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
public static function ggg(){
    $ddd = Sale::where('active','0')->get();
    return $ddd;
}
    protected $guarded = [];
    public function getProductForSale(){
        return $this->hasMany('App\Temp','id_sale');
    }

    public function getCustomerForSale(){
        return $this->belongsTo('App\Customer','fk_customer','id');
    }
    public function getEmpForSale(){
        return $this->belongsTo('App\User','fk_user','id');
    }
}
