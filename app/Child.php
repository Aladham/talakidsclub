<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $table = "child";
    protected $guarded=[];

    public function get_customer_child(){
        return $this->hasMany('app\Child','fk_parent_id');
    }
}
