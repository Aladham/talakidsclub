<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=[];

    public static function getProductType(){
        return ProductType::all();
    }
    public static function getProduct(){
        return Product::all();
    }

    public static function getProductTypeParty(){
        return ProductType::all()->where('parties',1);
    }

    public static function getTax(){
        return Tax::all();
    }

    public function getProductTypeForProduct(){
        return $this->belongsTo('App\ProductType','fk_product_type_id');
    }

}
