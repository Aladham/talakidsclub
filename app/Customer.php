<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
//    use SoftDeletes;
    protected $guarded = [];
//    protected $data = ['deleted_at'];

    public static function getCustomer($id){
        $data = Customer::find($id);
        return $data;
    }
    public function getItemForCustomer(){
        return $this->hasMany('App\Temp','fk_customer_id');
    }

    public function getChildCustomer(){
        return $this->hasMany('App\Child','fk_parent_id');
    }

    public static function getVisits($id){
        $data = CustomerPoints::orderBy('created_at', 'desc')->where('fk_customer_id',$id)->get();
        return $data;
    }
}
