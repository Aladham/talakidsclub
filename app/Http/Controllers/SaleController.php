<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Temp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAllSale()
    {
//        $data = Sale::all();
        $data = Sale::orderBy('id', 'DESC')->where('active', '0')->paginate(100);
        $data_deactive = Sale::orderBy('id', 'DESC')->where('active', '1')->paginate(100);
        return view('sale.sales', compact('data', 'data_deactive'));
    }

    public function getSale($id)
    {
        $data = Sale::where('id', $id)->first();
        $data_product = Temp::where('id_sale', $id)->get();
        return view('sale.sale', compact('data', 'data_product'));
    }
    public function editSale($id)
    {
        $data = Sale::where('id', $id)->first();
        return view('sale.sale-edit', compact('data'));
    }
    public function updateSale($id)
    {
        Sale::find($id)->update([
            'payment' => request('payment'),
        ]);
        return redirect()->route('DataSale',$id);
    }

    public function getSaleTax($id)
    {
        $data = Sale::where('order_s', $id)->where('active', 0)->first();
        if ($data) {
            $data_product = Temp::where('id_sale', $id)->get();
            return view('sale.sale', compact('data', 'data_product'));
        } else {
            return redirect()->route('sale');
        }
    }


    public function hideSale($id)
    {
        //  $data =  Sale::where('id',$id)->get();
        Sale::find($id)->update([
            'active' => 1,
        ]);
        $data_id = Sale::where('active', '0')->pluck('id')->toArray();
        $data = Sale::find($data_id);
        $index = 1;
        foreach ($data as $temp) {
            $temp->update([
                'order_s' => $index,
            ]);
            $index++;
        }
        return redirect()->back();
        // echo json_encode($data);
    }

    public function activeSale($id)
    {
        Sale::find($id)->update([
            'active' => 0,
        ]);
        $data_id = Sale::where('active', '0')->pluck('id')->toArray();
        $data = Sale::find($data_id);
        $index = 1;
        foreach ($data as $temqp) {
            $temqp->update([
                'order_s' => $index,
            ]);
            $index++;
        }
        return redirect()->back();
    }

    public function hideMultiSale()
    {
        $ids_req = request('checkSale');
        $ids = Sale::find($ids_req);
        if (!empty($ids_req)) {
            foreach ($ids as $id) {
                $id->update([
                    'active' => 1,
                ]);
            }
        }
        $data_id = Sale::where('active', '0')->pluck('id')->toArray();
        $data = Sale::find($data_id);
        $index = 1;
        foreach ($data as $temp) {
            $temp->update([
                'order_s' => $index,
            ]);
            $index++;
        }
        return redirect()->back();
    }

    public function searchSale()
    {


//            $data = Sale::where('active', '0')->where(function ($query) use ($data_req) {
//                $query->whereDate("created_at", $data_req);
//                if (Auth::user()->type == 0) {
//                    $query->orWhere('id', $data_req);
//                } else {
//                    $query->orWhere('order_s', $data_req);
//                }
//                $query->orWhereHas('getCustomerForSale', function ($q) use ($data_req) {
//                    $q->where(function ($q) use ($data_req) {
//                        $q->where('last_name', 'LIKE', $data_req);
//                    });
//                });
//
//            })->get();

        $from = request('from');
        $to = request('to');

        $data = Sale::orderBy('order_s', 'desc')
            ->where('active', '0')
            ->whereBetween("created_at", [$from .' 00:00:00',$to  .' 23:59:59'])->get();



        return view('sale.sales-result', compact('data','from','to'));
    }

    public function saleDay()
    {
        $data_req = request('data');
        $data = Sale::orderBy('order_s', 'desc')->where('active', '0')->whereDate("created_at", $data_req)->get();
        $cash = Sale::where('active', '0')->where('payment', 'Cash')->whereDate("created_at", $data_req)->get();
        $visa = Sale::where('active', '0')->where('payment', 'Visa')->whereDate("created_at", $data_req)->get();
        $data_deactive = Sale::where('active', '1')->whereDate("created_at", $data_req)->get();
        $totalCash = 0;
        $totalVisa = 0;
        foreach ($cash as $a) {
            $totalCash += $a->total;
        }
        foreach ($visa as $b) {
            $totalVisa += $b->total;
        }
        return view('sale.sale-day', compact('data', 'data_deactive', 'totalCash', 'totalVisa','data_req'));
    }
    public function saleDays()
    {
        $form = request('form');
        $to = request('to');

        $data = Sale::orderBy('order_s', 'desc')->where('active', '0')->whereBetween("created_at", [$form .' 00:00:00',$to  .' 23:59:59'])->get();
        $cash = Sale::where('active', '0')->where('payment', 'Cash')->whereBetween("created_at", [$form .' 00:00:00',$to  .' 23:59:59'])->get();
        $visa = Sale::where('active', '0')->where('payment', 'Visa')->whereBetween("created_at", [$form .' 00:00:00',$to  .' 23:59:59'])->get();
//        $data_deactive = Sale::where('active', '1')->whereDate("created_at", $data_req)->get();
        $totalCash = 0;
        $totalVisa = 0;
        foreach ($cash as $a) {
            $totalCash += $a->total;
        }
        foreach ($visa as $b) {
            $totalVisa += $b->total;
        }
        return view('sale.sale-days', compact('data',  'totalCash', 'totalVisa'));
    }


    public function deleteSaleDay(){
        $date = request('date');
        Sale::where('active','1')->whereDate("created_at", $date)->delete();
        return redirect()->back();
    }

}
