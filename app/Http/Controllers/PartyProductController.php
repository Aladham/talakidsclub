<?php

namespace App\Http\Controllers;

use App\PartyProduct;
use Illuminate\Http\Request;

class PartyProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getPartyProduct(){
        $data = PartyProduct::all();
        return view('product.party-product', compact('data'));
    }

    public function AddPartyProduct()
    {
        $this->validate(request(), [
            'name' => 'required',
            'price' => 'required',
            'size' => 'required',
        ], [
            'name.required' => 'Please Enter Name',
            'price.required' => 'Please Enter Price ',
            'size.required' => 'Please Enter Size ',
        ]);

        PartyProduct::create([
            'name' => request('name'),
            'description' => request('description'),
            'fk_product_type_id' => request('ProductType'),
            'price' => request('price'),
            'deposit' => request('deposit'),
            'tax' => request('tax'),
            'size' => request('size'),
            'price_per_extra_child' => request('price_per_extra_child'),
            'duration' => request('duration'),
            'order_p' => request('order'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }

    public function deletePartyProduct($id)
    {
        $product_type = PartyProduct::find($id);
        $product_type->delete();
        session()->flash('message', 'Successfully Deleted!');
        return redirect()->back();
    }
    public function editPartyProduct($id)
    {
        $data = PartyProduct::find($id);
        return view('product.edit-party', compact('data'));

    }
    public function updatePartyProduct()
    {
        PartyProduct::find(request('id'))->update([
            'name' => request('name'),
            'description' => request('description'),
            'fk_product_type_id' => request('ProductType'),
            'price' => request('price'),
            'deposit' => request('deposit'),
            'tax' => request('tax'),
            'size' => request('size'),
            'price_per_extra_child' => request('price_per_extra_child'),
            'duration' => request('duration'),
            'order_p' => request('order'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->route('getPartyProduct');
    }
}
