<?php

namespace App\Http\Controllers;

use App\Events;
use Carbon\Carbon;
use Illuminate\Http\Request;
use IntlCalendar;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function parties()
    {
        $parties = Events::orderBy('from_party','DESC')->get();
        $parties_list = [];
        foreach ($parties as $key => $party) {
            $from = new \DateTime($party->from_party);
            $to = new Carbon($party->to_party);
            $time_a = $from->format('H');
            $parties_list[] = Calendar::event(
                $party->party_title . ' (' . $from->format('H:i') . '-' . $to->format('H:i') . ')',
                true,
                new \DateTime($party->date_party),
                new \DateTime($party->date_party),
                $party->id, //optional event ID
                [
                    'url' => route('viewParty',$party->id)
                ]


            );
        }
        $calendar_details = Calendar::addEvents($parties_list)->setOptions(['header' => [
            'left' => 'today prev,next',
            'center' => 'title',
            'right' => ''],
        ]);
        return view('parties.parties', compact('calendar_details'));
    }

    public function addParty()
    {
        $this->validate(request(), [
            'party_title' => 'required',
            'date_party' => 'required',
            'from_party' => 'required',
            'to_party' => 'required',
            'contact_name' => 'required',
            'phone' => 'required',
            'number_kids' => 'required',
            'price' => 'required',
        ]);

        Events::Create([
            'party_title' => request('party_title'),
            'party_details' => request('party_details'),
            'date_party' => request('date_party'),
            'from_party' => request('from_party'),
            'to_party' => request('to_party'),
            'contact_name' => request('contact_name'),
            'phone' => request('phone'),
            'number_kids' => request('number_kids'),
            'price' => request('price'),
        ]);
        session()->flash('message', 'Successfully Added!');
        return redirect()->route('parties');
    }

    public function viewParty($id)
    {
        $party = Events::find($id);
        if ($party) {
            return view('parties.viewParty', compact('party'));
        } else {
            return redirect()->route('sale');
        }
    }
    public function editParty($id)
    {
        $party = Events::find($id);
        if ($party) {
            return view('parties.editParty', compact('party'));
        } else {
            return redirect()->route('sale');
        }
    }


    public function updateParty($id){
        $this->validate(request(), [
            'party_title' => 'required',
            'date_party' => 'required',
            'from_party' => 'required',
            'to_party' => 'required',
            'contact_name' => 'required',
            'phone' => 'required',
            'number_kids' => 'required',
            'price' => 'required',
        ]);

        $party = Events::find($id);
        $party->update([
            'party_title' => request('party_title'),
            'party_details' => request('party_details'),
            'date_party' => request('date_party'),
            'from_party' => request('from_party'),
            'to_party' => request('to_party'),
            'contact_name' => request('contact_name'),
            'phone' => request('phone'),
            'number_kids' => request('number_kids'),
            'price' => request('price'),
        ]);
        session()->flash('message', 'Successfully Updated!');
        return redirect()->back();
    }
    public function deleteParty($id){
        $party = Events::find($id);
        if ($party){
            $party->delete();
            return redirect()->route('parties');
        }else{
            return redirect()->route('sale');
        }
    }
}
