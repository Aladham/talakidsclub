<?php

namespace App\Http\Controllers;

use App\Tax;
use Illuminate\Http\Request;

class TaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTax(){
        $data = Tax::all();
        return view('tax.tax',compact('data'));
    }

    public function addTaxRate()
    {
        $this->validate(request(), [
            'tax_rate' => 'required',
            'amount' => 'required',
        ], [
            'tax_rate.required' => 'Please Enter Tax Rate',
            'amount.required' => 'Please Enter Amount',
        ]);

        Tax::create([
            'tax_rate' => request('tax_rate'),
            'amount' => request('amount'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }

    public function deleteTaxRate($id)
    {
        $tax_rate = Tax::find($id);
        $tax_rate->delete();
        return redirect()->back();
    }

    public function editTaxRate($id)
    {
        $data = Tax::find($id);
        return view('tax.edit-tax', compact('data'));

    }

    public function updateTaxRate()
    {
        Tax::find(request('id'))->update([
            'tax_rate' => request('tax_rate'),
            'amount' => request('amount'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }
}
