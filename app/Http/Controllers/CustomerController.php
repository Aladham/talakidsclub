<?php

namespace App\Http\Controllers;

use App\Child;
use App\Customer;
use App\CustomerPoints;
use App\Sale;
use App\Temp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /*** Function View add customer page ***/


    public function view_add_customer()
    {
        return view('customer.add-customer');
    }
    /*** Function View add Child page ***/

    /*** Function get customers ***/
    public function getCustomers()
    {
        $data = Customer::orderBy('id', 'desc')->paginate(100);

        // $data = Customer::onlyTrashed()->get();

        return view("customer.customers", compact('data'));
    }
//    public function getCustomers()
//    {
//        $data = Customer::orderBy('id', 'desc')->select('id','first_name','last_name','primary_phone','secondary_phone','point','notes');
//        return datatables($data)->make(true);
//    }
//    public function getDeletedCustomers()
//    {
//        $data = Customer::onlyTrashed()->get();
//        return view("customer.ActivateCustomers", compact('data'));
//    }

    public function addCustomer()
    {
        $this->validate(request(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'primary_phone' => 'required|unique:customers',
        ], [
            'firstName.required' => 'Please Enter First Name',
            'lastName.required' => 'Please Enter Last Name',
            'primary_phone.required' => 'Please Enter Primary Phone',
            'primary_phone.unique' => 'Primary Phone Already Existing',
        ]);

        $data = Customer::create([
            'first_name' => request('firstName'),
            'last_name' => request('lastName'),
            'primary_phone' => request('primary_phone'),
            'secondary_phone' => request('secondaryPhone'),
            'notes' => request('notes'),
        ]);
//        session()->flash('message', 'Successfully Added !');
        return redirect()->route('viewCustomer', $data->id);
    }

    public function deleteCustomer($id)
    {
        $child = Child::where('fk_parent_id', $id)->get();
        $sales = Sale::where('fk_customer', $id)->get();
        if (count($child) > 0 || count($sales) > 0) {
            session()->flash('error_message', 'Please delete all sales records and child names first!');
            return redirect()->back();
        } else {
            $Customer = Customer::find($id);
            if ($Customer) {
                $Customer->delete();
                session()->flash('message', 'Successfully Deleted!');
                return redirect()->back();
            } else {
                return redirect()->route('sale');
            }
        }
    }

    public function deleteAllSaleCustomer($id){
      $sales = Sale::where('fk_customer',$id);
        if ($sales) {
            $sales->delete();
            $data_id = Sale::where('active', '0')->pluck('id')->toArray();

            $data = Sale::find($data_id);
            $index = 1;
            foreach ($data as $temp) {
                $temp->update([
                    'order_s' => $index,
                ]);
                $index++;
            }
            session()->flash('message', 'Successfully Deleted!');
            return redirect()->back();
        }else {
            return redirect()->route('sale');
        }

    }
    public function deleteVisit($id){
        $visit = CustomerPoints::find($id);
        if ($visit) {
            $visit->delete();
            return redirect()->back();
        }else {
            return redirect()->route('sale');
        }
    }
    public function deleteSaleCustomer($id){
        $sales = Sale::find($id);
        $temps = Temp::find($id);
        if ($sales) {
            $sales->delete();
//            $data_id = Temp::where('id_sale', $id)->pluck('id')->toArray();
//            foreach ($data_id as $temp_id){
//                $temp_id->delete();
//            }
            $data_id = Sale::where('active', '0')->pluck('id')->toArray();
            $data = Sale::find($data_id);
            $index = 1;
            foreach ($data as $temp) {
                $temp->update([
                    'order_s' => $index,
                ]);
                $index++;
            }
            session()->flash('message', 'Successfully Deleted!');
            return redirect()->back();
        }else {
            return redirect()->route('sale');
        }

    }

    public function editCustomer($id)
    {

        $data = Customer::find($id);
        if ($data) {
            return view('customer.edit-customer', compact('data'));
        } else {
            return redirect()->route('sale');
        }
    }

    public function viewCustomer($id)
    {

        $data = Customer::find($id);
        if ($data) {
            $data_child = Child::where("fk_parent_id", "=", $id)->get();
            if (Auth::user()->type == 0){
                $data_sales = Sale::orderBy('created_at', 'desc')->where("fk_customer", "=", $id)->get();
            }else{
                $data_sales = Sale::orderBy('created_at', 'desc')->where("fk_customer", "=", $id)->where('active', 0)->get();
            }

            return view('customer.customer', compact('data', 'data_child', 'data_sales'));
        } else {
            return redirect()->route('sale');
        }
    }

    public function updateCustomer()
    {
        Customer::find(request('id'))->update([
            'first_name' => request('firstName'),
            'last_name' => request('lastName'),
            'primary_phone' => request('primaryPhone'),
            'secondary_phone' => request('secondaryPhone'),
            'point' => request('point'),
            'notes' => request('notes'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }

    public function usePoint()
    {
        $data_customer = Customer::where('id', request('id_c'))->first();

//        if (request('point') > $data_customer->point) {
//            session()->flash('Errormessage', 'The number of points must be less than ' . $data_customer->point . '!');
//            return redirect()->back();
//        } else {
        $point = request('total_point');
        $print_visit =$data_customer->point - $point;
        Customer::find(request('id_c'))->update([
            'point' => $point,
            'notes' => request('notes'),
        ]);
        $visit =( $data_customer->point - request('total_point')) / 10 ;
        CustomerPoints::Create([
            'points'=>$visit,
            'fk_customer_id'=>request('id_c'),
        ]);
        $data_point = Customer::where('id', request('id_c'))->first();

//        $final_point = request('point');

        if (!empty(request('child'))) {
            $childs[] = request('child');
        }

        return view('point', compact('data_customer', 'print_visit', 'data_point', 'childs'));

    }

    public function activateCustomer()
    {
        Customer::withTrashed()->find(request('id'))->restore();
        return redirect()->back();
    }

    public function searchCustomers()
    {
        if (!empty(request('data'))) {
            $str = request('data');
            $str[0];
            if ($str[0] == "#") {
                $rest = substr($str, 1);
                $data = Customer::Where("id", "like", $rest)->get();
            } else {
                $data = Customer::Where("first_name", "like", '%' . request('data') . '%')
                    ->orWhere("last_name", "like", '%' . request('data') . '%')
                    ->orWhere("primary_phone", "like", '%' . request('data') . '%')
                    ->orWhere("secondary_phone", "like", '%' . request('data') . '%')
                    ->orWhere("id", "like", request('data'))
                    ->get();
            }
            return view('customer.customers-result', compact('data'));

        } else {
            $data = Customer::limit();
            return view('customer.customers-result', compact('data'));
        }
    }

    public function ViewAddChild($id)
    {
        return view('customer.add-child', compact('id'));
    }

    public function AddChild($id)
    {
        $this->validate(request(), [
            'name' => 'required',
            'gender' => 'required',
        ], [
            'name.required' => 'Please Enter Name',
            'gender.required' => 'Please Enter Gender ',
        ]);

        Child::create([
            'name' => request('name'),
            'gender' => request('gender'),
            'fk_parent_id' => request('id'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->route('viewCustomer', $id);
    }

    public function deleteChild($id)
    {
        $child = Child::find($id);
        if ($child) {
            $child->delete();
            session()->flash('message', 'Successfully Deleted!');
            return redirect()->back();
        } else {
            return redirect()->route('sale');
        }
    }

    public function editChild($id)
    {
        $data = Child::find($id);
        if ($data) {
            return view('customer.edit-child', compact('data'));
        } else {
            return redirect()->route('sale');
        }

    }

    public function updateChild()
    {
        Child::find(request('id'))->update([
            'name' => request('name'),
            'gender' => request('gender'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }


}
