<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewEmployees()
    {
        $data = User::all();
        return view('employee.employees', compact('data'));
    }


    public function addEmployee()
    {
        $this->validate(request(), [
            'username' => 'required|unique:users',
            'password' => 'required|min:6',
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required|unique:users',
        ], [
            'username.required' => 'Please Enter Username',
            'username.unique' => 'This Username Already Exists',
            'firstName.required' => 'Please Enter First Name',
            'lastName.required' => 'Please Enter Last Name',
            'phone.required' => 'Please Enter Phone',
            'phone.unique' => 'This Phone Already Exists',
        ]);


        User::create([
            'username' => request('username'),
            'first_name' => request('firstName'),
            'last_name' => request('lastName'),
            'password' => bcrypt(request('password')),
            'type' => request('employeeType'),
            'phone' => request('phone'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }

    public function deleteEmployee($id)
    {
        $user = User::find($id);
        $user->delete();
        session()->flash('message', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function editEmployee($id)
    {
        $data = User::find($id);
        return view('employee.edit-employee', compact('data'));

    }

    public function updateEmployee()
    {

        $this->validate(request(), [
            'username' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
        ], [
            'username.required' => 'Please Enter Username',
            'firstName.required' => 'Please Enter First Name',
            'lastName.required' => 'Please Enter Last Name',
            'phone.required' => 'Please Enter Phone',
        ]);

        $input = request()->all();
        if (request()->Input('password')) {
            $bcrypt_pass = bcrypt(request()->Input('password'));
            $input['password'] = $bcrypt_pass;
        } else {
            unset($input['password']);
        }

        $data = User::find(request('id'));
        $current_password = $data->password;
        $new_password = request('password');
        if (!empty(request('password'))) {
            $new_password = bcrypt(request('password'));
        } else {
            $new_password = $current_password;
        }
        User::find(request('id'))->update([
            'username' => request('username'),
            'type' => request('employeeType'),
            'phone' => request('phone'),
            'password' => $new_password,
            'first_name' => request('firstName'),
            'last_name' => request('lastName'),
        ]);
        session()->flash('message', 'Successfully Update');
        return redirect()->back();
    }


}
