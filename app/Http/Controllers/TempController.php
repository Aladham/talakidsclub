<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerPoints;
use App\Sale;
use App\SaleTab;
use App\Temp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TempController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function createSale($id = NULL)
    {
        $data = SaleTab::orderBy('order')->where('active', '=', '0')->get();

        if ($id != NULL || $id != '') {
            $temps = Temp::where([['fk_customer_id', $id], ['active', 0]])->get();

        } else {
            $temps = Temp::where([['fk_customer_id', NULL], ['fk_users_id', Auth::user()->id], ['active', 0]])->get();
        }

        $sub_total = 0;
        $tax_total = 0;
        $total_point = 0;
        $total = 0;
        if (count($temps) > 0) {
            foreach ($temps as $row) {
                $total_price = $row->price * $row->quantity;
                $discount = $row->discount;
                $point = $row->point;
                if ($discount < 1 && $discount > -1) {
                    $final_price = $total_price - ($total_price * $discount);
                }
                if ($discount >= 1) {
                    $final_price = $total_price - $discount;
                }
                if ($discount == -1) {
                    $final_price = $total_price - $total_price;
                }
                $tax = $row->tax;
                $sum_tax = $final_price * $tax;
                $sub_total += $final_price;
                $tax_total += $sum_tax;
                $total_point += $point;

//                $precision2 = 2;
                $final_taxtotal = round($tax_total, 2);
                $final_subtotal = round($sub_total, 2);
                $final_taxtotal = sprintf('%0.2f', $final_taxtotal);
                $final_subtotal = sprintf('%0.2f', $final_subtotal);
            }
            $total = number_format($final_subtotal + $final_taxtotal, 2);

        }


        return view("sale", compact('data', 'id', 'final_subtotal', 'final_taxtotal', 'total', 'total_point', 'final_price'));


    }


    public function deleteAllSaleTemp($id)
    {
        $data = SaleTab::orderBy('order')->where('active', '=', '0')->get();
        $temps = Temp::where([['fk_customer_id', '=', $id], ['active', '=', 0]])->get();
        foreach ($temps as $temp) {
            $temp->delete();

        }
        return view("sale", compact('data', 'id'));
    }

    public function deleteAllSaleTempForAuth($id = NULL)
    {

        $data = SaleTab::orderBy('order')->where('active', '=', '0')->get();
        $temps = Temp::where([['fk_users_id', Auth::user()->id], ['active', 0]])->get();
        foreach ($temps as $temp) {
            $temp->delete();
        }
        return view("sale", compact('data', 'id'));

    }

    public function addSaleTemp()
    {
        Temp::create([
            'item' => request('item'),
            'price' => request('price'),
            'tax' => request('tax'),
            'point' => request('point'),
            'quantity' => 1,
            'discount' => 0,
            'fk_customer_id' => request('customer_id'),
            'fk_users_id' => request('users_id'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->route('sale', request('customer_id'));
    }

    public function deleteSaleTemp($id)
    {
        $temp = Temp::find($id);
        if($temp) {
            $temp->delete();
        }
        return redirect()->back();
    }

    public function addDiscountItem()
    {
        $temps = Temp::find(request('item'));
        if (!empty(request('item'))) {
            foreach ($temps as $temp) {
                $temp->update([
                    'discount' => request('discount'),
                    'discount_reasons' => request('discount_reasons'),
                ]);
            }
        }
        return redirect()->back();
    }

    public function addQuantityItem()
    {
        $temps = Temp::find(request('id_item_modal'));
        if (request('quantity_modal') == 0) {
            $temps->update([
                'quantity' => 1,
            ]);
        } else {
            $temps->update([
                'quantity' => request('quantity_modal'),
            ]);
        }
        return redirect()->back();
    }


    public function depositSale()
    {
        $last_number = Sale::where('active', 0)->orderBy('order_s', 'DESC')->first();
        if ($last_number) {
            $last_id = $last_number->order_s + 1;
        }else{
            $last_id = 1;
        }
        $change = request('cash_received') - request('s_total');
        $childs2 = request('child');

        $data = Sale::create([
            'sub_total' => request('sub_total'),
            'tax' => request('taxtotal'),
            'total' => request('s_total'),
            'fk_customer' => request('id_customer'),
            'fk_user' => Auth::user()->id,
            'payment' => request('payment_m'),
            'cash_received' => request('cash_received'),
            'change_p' => abs($change),
            'order_s' => $last_id,
            'note' => request('sale_notes'),
            'child' => json_encode($childs2),
        ]);
        $temps = Temp::find(request('payment_id_item'));
        if (!empty(request('payment_id_item'))) {
            foreach ($temps as $temp) {
                $temp->update([
                    'active' => 1,
                    'id_sale' => $data->id,
                ]);
            }
        }
        $data_sale = Sale::where('id', $data->id)->get();
        $data_customer = Customer::where('id', $data->fk_customer)->first();
        if (request('t_point') != request('total_point')){
        $visit =(request('t_point') - request('total_point')) / 10 ;
        CustomerPoints::Create([
            'points'=>$visit,
            'fk_customer_id'=>request('id_customer'),
        ]);
        }
        /*** Add Point for Customer***/
        if (!empty(request('id_customer'))) {
            $point_customer = $data_customer->point;
            //  $point = $point_customer - request('total_point');
            $point = request('total_point') + $point_customer;
            Customer::find(request('id_customer'))->update([
                'point' => $point,
            ]);
        }
        if (!empty(request('child'))) {
            $childs[] = request('child');
        }
        return view('invoice', compact('data_sale', 'data', 'data_customer', 'childs'));
    }

    public function Receipts($id)
    {
        $data_sale = Sale::where('id', $id)->first();
        $data_customer = Customer::where('id', $data_sale->fk_customer)->first();

        return view('receipts', compact('data_sale', 'data', 'data_customer'));
    }
}
