<?php

namespace App\Http\Controllers;

use App\Discount;
use App\DiscountReason;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDiscount(){
        $data = Discount::all();
        return view('discount.discount',compact('data'));
    }

    public function addDiscount()
    {
        $this->validate(request(), [
            'amount' => 'required',
        ], [
            'amount.required' => 'Please Enter Amount',
        ]);

        if(is_numeric(request('amount'))) {
            Discount::create([
                'amount' => request('amount'),
            ]);
            session()->flash('message', 'Successfully Added!');
        }
        else{
            session()->flash('message_error', 'Error Please Insert Numbe!');
        }
        return redirect()->back();
    }

    public function deleteDiscount($id)
    {
        $discount = Discount::find($id);
        $discount->delete();
        return redirect()->back();
    }

    public function editDiscount($id)
    {
        $data = Discount::find($id);
        return view('discount.edit-discount', compact('data'));

    }

    public function updateDiscount()
    {
        if(is_numeric(request('amount'))) {
            Discount::find(request('id'))->update([
                'amount' => request('amount'),
            ]);
            session()->flash('message', 'Successfully Update !');
        }
        else{
            session()->flash('message_error', 'Error Please Insert Number!');
        }
        return redirect()->back();
    }

    public function getDiscountReason(){
        $data = DiscountReason::all();
        return view('discount.discount-reason',compact('data'));
    }

    public function addDiscountReason()
    {
        $this->validate(request(), [
            'reason' => 'required',
        ], [
            'reason.required' => 'Please Enter Amount',
        ]);

        DiscountReason::create([
            'reason' => request('reason'),
        ]);
        session()->flash('message', 'Successfully Added!');
        return redirect()->back();
    }

    public function deleteDiscountReason($id)
    {
        $discount = DiscountReason::find($id);
        $discount->delete();
        return redirect()->back();
    }

    public function editDiscountReason($id)
    {
        $data = DiscountReason::find($id);
        return view('discount.edit-discount-reason', compact('data'));

    }

    public function updateDiscountReason()
    {
        DiscountReason::find(request('id'))->update([
            'reason' => request('reason'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }
}
