<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewReports()
    {
        return view('reports.reports');
    }

    public function dailySummaryReport()
    {
        $start_date = request('start_date');
        $end_date = request('end_date');
        $product_Types_Summary = DB::select('SELECT
            
              SUM(temps.quantity) AS Quantity,
              sale_tabs.name AS Sale_Tab,
              product_types.name AS Product_Types,
              temps.item AS Product,
                  SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) asPreTax,
                  SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) * temps.tax AS SalesTax,
                  SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) * (temps.tax + 1) AS Total
            FROM temps,
                 products,
                 product_types,
                 sale_tabs,
                sales
 
            WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
            AND item = products.name
            AND products.fk_product_type_id = product_types.id
            AND product_types.fk_sale_tab_id = sale_tabs.id
            AND temps.id_sale = sales.id
            AND sales.active = 0
            GROUP BY product_types.name');
        $products_Summary = DB::select('SELECT
  SUM(temps.quantity) AS Quantity,
  sale_tabs.name AS Sale_Tab,
  product_types.name AS Product_Types,
  temps.item,
  SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) asPreTax,
  SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) * temps.tax AS SalesTax,
  SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) * (temps.tax + 1) AS Total
FROM temps,
     products,
     product_types,
     sale_tabs,
     sales
 WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
AND item = products.name
AND products.fk_product_type_id = product_types.id
AND product_types.fk_sale_tab_id = sale_tabs.id
AND sales.id = temps.id_sale
AND sales.active = 0
GROUP BY temps.item');
        $total_total = 0;
        foreach ($product_Types_Summary as $sum_total) {
            $total_total += $sum_total->Total;
        }
        $total_total2 = 0;
        foreach ($products_Summary as $sum2_total) {
            $total_total2 += $sum2_total->Total;
        }
        $sale_tab_summary = DB::select('SELECT
                              sale_tabs.name AS Sale_Table,
                             
                              
                              SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) as Pre_Tax,
  SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) * temps.tax AS Sales_Tax,
  SUM(temps.price - (CASE
    WHEN temps.discount = -1 THEN temps.price
    WHEN temps.discount < 1 THEN temps.price * temps.discount
    ELSE temps.discount
  END)) * (temps.tax + 1) AS Total
  
                            FROM temps,
                                 products,
                                 product_types,
                                 sale_tabs,
                                 sales
                            WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
                            AND item = products.name
                            AND products.fk_product_type_id = product_types.id
                            AND product_types.fk_sale_tab_id = sale_tabs.id
                             AND temps.id_sale = sales.id
            AND sales.active = 0
                            GROUP BY sale_tabs.name
        ');

        $payment_types_summary = DB::select('SELECT
                  COUNT(sales.payment) AS Quantity,
                  sales.payment AS Payment_Type,
                   SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) asPreTax,
                  SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) * temps.tax AS SalesTax,
                  SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) * (temps.tax + 1) AS Amount 
                  
                FROM temps,
                     sales
                WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
                AND temps.id_sale = sales.id
                AND sales.active = 0
                GROUP BY sales.payment');

        $tax_summary = DB::select('SELECT sale_tabs.name AS Tab_name,
              temps.tax AS Tax_Type,
             
             SUM(temps.price - (CASE
                    WHEN temps.discount = -1 THEN temps.price
                    WHEN temps.discount < 1 THEN temps.price * temps.discount
                    ELSE temps.discount
                  END)) * (temps.tax) AS Amount 
            FROM temps,
            sales,
              sale_tabs,
            product_types,
            products
            WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
            AND temps.id_sale = sales.id AND item = products.name 
            AND products.fk_product_type_id = product_types.id 
            AND product_types.fk_sale_tab_id=sale_tabs.id 
            AND sales.active = 0 GROUP BY sale_tabs.name
            ');

        $voided_sales = DB::select('
                    SELECT
              sales.id AS Sale_Id,
              temps.item AS Product,
              sales.payment AS Payment_Type,
              temps.price * (temps.tax + 1) AS Total
            FROM temps,
                 sales
            WHERE temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
            AND temps.id_sale = sales.id
            AND sales.active = 1
                    ');

        return view('reports.ReportDailySummary', compact('start_date', 'end_date', 'product_Types_Summary','products_Summary', 'total_total','total_total2', 'sale_tab_summary', 'tax_summary'
            , 'payment_types_summary', 'voided_sales'
        ));
    }

    public function summaryBey()
    {
        $start_date = request('start_date');
        $end_date = request('end_date');
        $summaryAccount = DB::select('SELECT distinct DATE(created_at) as Date, 
         SUM(IF(sales.payment = \'CASH\', sales.total,0)) as totalCash, 
          SUM(IF(sales.payment = \'VISA\', sales.total,0)) as totalVISA
        FROM   
        sales  Where  sales.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\' 
        and sales.active = 0
        GROUP BY  DATE(sales.created_at)');
        return view('reports.summaryBey', compact('start_date', 'end_date', 'summaryAccount'));
    }

//    public function salesReportByProduct()
//    {
//        $start_date = request('start_date');
//        $end_date = request('end_date');
//        $product = request('product');
//        $product_type = request('product_type');
//        $data = DB::select( '
//        SELECT
//  temps.item AS Product_Name,
//  sale_tabs.name AS Sale_Tab,
//  product_types.name
//  AS Product_Types,
//  SUM(temps.quantity) AS Quantity,
//  SUM(temps.price - (CASE
//    WHEN
//      temps.discount = -1 THEN temps.price
//    WHEN temps.discount < 1 THEN temps.price * temps.discount
//    ELSE temps.discount
//  END)) AS Sales
//FROM sales,
//     temps,
//     products,
//     product_types,
//     sale_tabs
//WHERE temps.item LIKE \'%' . $product . '%\' AND product_types.name like \'%' . $product_type . '%\'
//AND temps.created_at BETWEEN \'' . $start_date . ' 00:00:00\'  AND \'' . $end_date . ' 23:59:59\'
//AND item = products.name
//AND products.fk_product_type_id =
//product_types.id
//AND product_types.fk_sale_tab_id = sale_tabs.id
//AND sales.id =
//temps.id_sale
//AND sales.active = 0
//GROUP BY temps.item
//');
//        return view('reports.salesRerportByProduct', compact('start_date', 'end_date', 'data'));
//    }
}
