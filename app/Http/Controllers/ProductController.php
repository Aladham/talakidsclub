<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getProduct(){
        $data = Product::orderBy('position')->get();
        return view('product.product', compact('data'));
    }

    public function productSearch(){
        $data = Product::all();
        return view('product.product-search', compact('data'));
    }
    public function addProduct()
    {
        $this->validate(request(), [
            'name' => 'required',
            'price' => 'required',
        ], [
            'name.required' => 'Please Enter Name',
            'price.required' => 'Please Enter Price ',
        ]);

        Product::create([
            'name' => request('name'),
            'fk_product_type_id' => request('ProductType'),
            'price' => request('price'),
            'tax' => request('tax'),
            'point' => request('point'),
            'position' =>0,
            'online_party' => 0,
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }

    public function deleteProduct($id)
    {
        $product_type = Product::find($id);
        $product_type->delete();
        session()->flash('message', 'Successfully Deleted!');
        return redirect()->back();
    }
    public function editProduct($id)
    {
        $data = Product::find($id);
        return view('product.edit-product', compact('data'));

    }
    public function updateProduct()
    {
        Product::find(request('id'))->update([
            'name' => request('name'),
            'fk_product_type_id' => request('ProductType'),
            'price' => request('price'),
            'tax' => request('tax'),
            'point' => request('point'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->route('getProduct');
    }
    public function position()
    {
        if (request('update')){
            foreach (request('positions') as $position){
                $index = $position[0];
                $newPosition = $position[1];

                Product::find($index)->update([
                    'position' => $newPosition,

                ]);
            }

        }
    }
}
