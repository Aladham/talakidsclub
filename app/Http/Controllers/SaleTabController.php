<?php

namespace App\Http\Controllers;

use App\ProductType;
use App\SaleTab;
use Illuminate\Http\Request;

class SaleTabController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getSaleTabs()
    {
        $data = SaleTab::all()->where('active', '=', '0');
        $deactivate = SaleTab::all()->where('active', '=', '1');
        return view("saletabs.sale-tabs", compact('data', 'deactivate'));
    }

    public function AddSaleTab()
    {

        $this->validate(request(), [
            'name' => 'required',
            'weight' => 'required',
        ], [
            'name.required' => 'Please Enter Name',
            'weight.required' => 'Please Enter Weight',
        ]);


        SaleTab::create([
            'name' => request('name'),
            'order' => request('weight'),

        ]);

//        $coustmer_1 = [
//            "products"=>[1,2,3],
//            "disaccount"=>[12]
//        ];
        session()->flash('message', 'Successfully Added !');

//        $response->withCookie(cookie()->forever('coustmer_1', $coustmer_1));

        return redirect()->back();
    }

    public function editSaleTab($id)
    {
        $data = SaleTab::find($id);
        if ($data) {
            return view('saletabs.edit-sale-tabs', compact('data'));
        } else {
            return redirect()->route('sale');
        }
    }

    public function deleteSaleTab($id)
    {
        $product_type = ProductType::where('fk_sale_tab_id',$id)->get();
        if (count($product_type) > 0){
            session()->flash('error_message', 'Please delete Product Type first!');
        }else{
            $sale_tab = SaleTab::find($id);
            if ($sale_tab) {
                $sale_tab->delete();
                session()->flash('messageDelete', 'Successfully Deleted!');

            } else {
                return redirect()->route('sale');
            }
        }
        return redirect()->back();
    }


    public static function get_tabs()
    {
        $data = SaleTab::where('active', '=', '0')->orderBy('order', 'ASC')->get();
        return view("sale", compact('data'));
    }

    public function updateSaleTab()
    {
        SaleTab::find(request('id'))->update([
            'name' => request('name'),
            'order' => request('weight'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }

    public function deactivateSaleTab()
    {
        SaleTab::find(request('id'))->update([
            'active' => '1',
        ]);
        return redirect()->back();
    }

    public function activateSaleTab()
    {
        SaleTab::find(request('id'))->update([
            'active' => '0',
        ]);
        return redirect()->back();
    }
}
