<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPayment(){
        $data = Payment::all();
        return view('payment-type.payment',compact('data'));
    }

    public function addPaymentType()
    {
        $this->validate(request(), [
            'payment_type' => 'required',
            'payment_name' => 'required',
        ], [
            'payment_type.required' => 'Please Enter Payment Type',
            'payment_name.required' => 'Please Enter Payment Name',
        ]);

        Payment::create([
            'payment_type' => request('payment_type'),
            'payment_name' => request('payment_name'),

        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }

    public function deletePaymentType($id)
    {
        $payment = Payment::find($id);
        $payment->delete();
        return redirect()->back();
    }

    public function editPaymentType($id)
    {
        $data = Payment::find($id);
        return view('payment-type.edit-payment', compact('data'));

    }

    public function updatePaymentType()
    {
        Payment::find(request('id'))->update([
            'payment_type' => request('payment_type'),
            'payment_name' => request('payment_name'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }
}
