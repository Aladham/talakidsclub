<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function productType()
    {
        $data = ProductType::all();
        return view('product.product-type', compact('data'));
    }

    public function addProductType()
    {
        $this->validate(request(), [
            'name' => 'required',
            'SaleTab' => 'required',
        ], [
            'name.required' => 'Please Enter Name',
            'SaleTab.required' => 'Please Chose Sale Tab ',
        ]);
        if (!empty(request('parties'))) {
            $parties = 1;
        } else {
            $parties = 0;
        }
        ProductType::create([
            'name' => request('name'),
            'fk_sale_tab_id' => request('SaleTab'),
            'parties' => $parties,
            'button_colour' => request('buttonColor'),
            'weight' => request('weight'),
        ]);
        session()->flash('message', 'Successfully Added !');
        return redirect()->back();
    }


    public function deleteProductType($id)
    {

        $product = Product::where('fk_product_type_id', $id)->get();
        if (count($product) > 0) {
            session()->flash('error_message', 'Please delete Products first!');
        } else {
            $product_type = ProductType::find($id);
            if ($product_type) {
                $product_type->delete();
                session()->flash('message', 'Successfully Deleted!');
            } else {
                return redirect()->route('sale');
            }
        }
        return redirect()->back();
    }

    public function editProductType($id)
    {
        $data = ProductType::find($id);
        return view('product.edit-product-type', compact('data'));

    }

    public function updateProductType()
    {
        ProductType::find(request('id'))->update([
            'name' => request('name'),
            'fk_sale_tab_id' => request('SaleTab'),
            'button_colour' => request('buttonColor'),
            'weight' => request('weight'),
        ]);
        session()->flash('message', 'Successfully Update !');
        return redirect()->back();
    }
}
