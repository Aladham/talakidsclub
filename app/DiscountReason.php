<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountReason extends Model
{
    protected $guarded = [];
    public static function getDiscountReasons(){
        $data = DiscountReason::all();
        return $data;
    }
}
